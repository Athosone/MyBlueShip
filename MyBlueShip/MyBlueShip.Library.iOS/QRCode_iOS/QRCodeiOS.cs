﻿using MyBlueShip.CrossLibrary;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UIKit;
using ZXing.Common;

namespace MyBlueShip.Library.iOS.QRCode_iOS
{
    class QRCodeiOS : IQRCodeServices<UIImage>
    {
        public async Task<string> ReadQRCode()
        {
            var scanner = new ZXing.Mobile.MobileBarcodeScanner();

            try
            {
                ZXing.Result result = await scanner.Scan();
                return result.Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }

       
        UIImage IQRCodeServices<UIImage>.WriteQRCode(string text, int width, int height)
        {
            var barcodeWriter = new ZXing.Mobile.BarcodeWriter
            {
                Format = ZXing.BarcodeFormat.QR_CODE,
                Options = new EncodingOptions
                {
                    Height = height,
                    Width = width,
                    Margin = 0
                }
            };
            try
            {
                UIImage image = barcodeWriter.Write(text);
                return image;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }
    }
}
