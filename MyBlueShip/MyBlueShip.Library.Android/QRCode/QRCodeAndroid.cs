using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;


using MyBlueShip.CrossLibrary;
using ZXing.Mobile;
using System.Threading.Tasks;
using ZXing.QrCode;
using ZXing.Common;
using ZXing;

namespace MyBlueShip.Library.Android.QRCode
{
    public class QRCodeAndroid : IQRCodeServices<Bitmap>
    {
        public Bitmap WriteQRCode(string text, int width, int height)
        {
            var barcodeWriter = new ZXing.Mobile.BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = new EncodingOptions
                {
                    Height = height,
                    Width = width,
                    Margin = 0
                }
            };
            Bitmap bmp = barcodeWriter.Write(text);
            return bmp;
        }

        async Task<string> IQRCodeServices<Bitmap>.ReadQRCode()
        {
            var scanner = new ZXing.Mobile.MobileBarcodeScanner();

            try
            {
                ZXing.Result result = await scanner.Scan();
                return result.Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }
    }
}