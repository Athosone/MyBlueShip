using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Provider;

namespace MyBlueShip.Library.Android.Design
{

    public enum AlertDialogType
    {
        NOGPS,
        NONFC,
        NOINTERNET
    }

    public class DesignComponent
    {

        #region AlertDialog

        public static AlertDialog CreateAlertDialog(Context ctx, int type)
        {
            switch (type)
            {
                case (int)AlertDialogType.NOGPS:
                    return CreateNoGpsAlertDialog(ctx);
                case (int)AlertDialogType.NONFC:
                    return CreateNoNfcAlertDialog(ctx);
                default:
                    return null;
            }
        }

        private static AlertDialog CreateNoNfcAlertDialog(Context ctx)
        {
            return DesignComponent.CreateAlertDialog(ctx, "NFC d�sactiv�", "Votre NFC est d�sactiv�, souhaitez-vous l'activer ?", "R�glages", "Non merci"
                 , (sender, args) =>
                 {
                     var intent = new Intent(Settings.ActionNfcSettings);
                     ctx.StartActivity(intent);
                     ((AlertDialog)sender).Cancel();
                 }, (sender, args) =>
                 {
                     ((AlertDialog)sender).Cancel();
                 });
        }

        public static AlertDialog CreateAlertDialog(Context ctx, string title, string message, string Ok, string Ko, EventHandler<DialogClickEventArgs> pos, EventHandler<DialogClickEventArgs> neg)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
            
            builder.SetTitle(title).SetMessage(message);
            builder.SetPositiveButton(Ok, pos);
            builder.SetNegativeButton(Ko, neg);
            return builder.Create();
         }

        private static AlertDialog CreateNoGpsAlertDialog(Context ctx)
        {
            return DesignComponent.CreateAlertDialog(ctx, "GPS d�sactiv�", "Votre GPS est d�sactiv�, souhaitez-vous l'activer ?", "R�glages", "Non merci"
                   , (sender, args) =>
                   {
                       var intent = new Intent(Settings.ActionLocationSourceSettings);
                       ctx.StartActivity(intent);
                       ((AlertDialog)sender).Cancel();
                   }, (sender, args) =>
                   {
                       ((AlertDialog)sender).Cancel();
                   });
        }

        #endregion

		#region ProgressDialog

		public static ProgressDialog CreateProgressDialog(Context ctx, string message, string title)
		{
			switch (MBSApi.Instance.appType) {

			case APP_Type.MOBMYBAG:
				return null;
			case APP_Type.MOBMYCAR:
				return MMCProgressDialog (ctx, message, title);
			default:
				return null;
			}
		}

		private static ProgressDialog MMCProgressDialog(Context ctx, string message, string title)
		{
			ProgressDialog progress = new ProgressDialog (ctx);
			progress.SetTitle (title);
			progress.SetMessage (message);
			return progress;
		}


		#endregion
    }
}