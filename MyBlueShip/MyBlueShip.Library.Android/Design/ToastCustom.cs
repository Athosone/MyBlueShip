using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MyBlueShip.Library.Android.Design
{
   public static class ToastCustom
    { 
		public static Toast MakeText(Activity act, string text, ToastLength duration, string textSub = "")
        { 
            ////
            //TODO Use MBSAPI.app_type pour savoir quelle image et couleur mettre
            ////
            View layout = act.LayoutInflater.Inflate(Resource.Layout.toastCustomLayout, null);

            TextView textV = (TextView)layout.FindViewById(Resource.Id.messageTextView);
			textV.SetText (text, TextView.BufferType.Normal);
            textV.Gravity = GravityFlags.Center;
			TextView textVSub = (TextView)layout.FindViewById(Resource.Id.messageSubTextView);
			textVSub.SetText (textSub, TextView.BufferType.Normal);
			textVSub.Gravity = GravityFlags.Center;
            ImageView imv = (ImageView)layout.FindViewById(Resource.Id.leftImageView);
			imv.SetImageResource(Resource.Drawable.icon_toast);
            Toast toast = new Toast(act.ApplicationContext);
			toast.SetGravity(GravityFlags.Center, 0, 0);
            toast.Duration = duration;
            toast.View = layout;
            return toast;
        }
    }
}