using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MyBlueShip.Library.Android.Events
{
    public enum StatusConnectivityType
    {
        NOGPS,
        NONFC,
        NOINTERNET
    }

    public class StatusConnectivityEventArgs : EventArgs
    {
        public StatusConnectivityType type { get; set; }
        public StatusConnectivityEventArgs(StatusConnectivityType type)
        {
            this.type = type;
        }
    }
}