using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace MyBlueShip.Library.Android.NFC
{
    class MBSProc_Var
    {

        // Variables pour le projet
        private static char[] hexArray = "0123456789ABCDEF".ToCharArray();

        // Fonction convertion byte[] vers chaine hexa
        public string bytesToHex(byte[] bytes)
        {
            char[] hexChars = new char[bytes.Length * 2];
            for (int j = 0; j < bytes.Length; j++)
            {
                int v = bytes[j] & 0xFF;
                //WAS >>>
                hexChars[j * 2] = hexArray[v >> 4];
                hexChars[j * 2 + 1] = hexArray[v & 0x0F];
            }
            return new string(hexChars);
        }

        // Renvoie la commande pour lecture de block NFCV
        public byte[] NFCV_ReadBlock(byte[] id, int BlockNo)
        {
            byte[] readCmd = new byte[3 + id.Length];
            readCmd[0] = 0x20; // set "address" flag (only send command to this tag)
            readCmd[1] = 0x20; // ISO 15693 Single Block Read command byte
            Array.Copy(id, 0, readCmd, 2, id.Length);//Copy id
            readCmd[2 + id.Length] = (byte)BlockNo;
            return readCmd;
        }

        // Renvoie la commande pour ecriture de block NFCV
        public byte[] NFCV_WriteBlock(int BlockNo, byte[] Data)
        {
            byte[] WriteCmd = new byte[7];
            WriteCmd[0] = 0x42;
            WriteCmd[1] = 0x21;
            WriteCmd[2] = (byte)BlockNo;
            WriteCmd[3] = Data[0];
            WriteCmd[4] = Data[1];
            WriteCmd[5] = Data[2];
            WriteCmd[6] = Data[3];

            return WriteCmd;
        }

        // Chaine vers tableau de byte[] 
        public byte[] StringToByteArray(string str)
        {
            byte[] DataBlock = new byte[str.Length / 2];
            string tmp;
            for (int i = 0; i <= (str.Length) / 2; i += 2)
            {
                tmp = str.Substring(i, i + 2);
                DataBlock[i] = (byte)Integer.ParseInt(tmp, 16);
            }
            return DataBlock;
        }
    }
}