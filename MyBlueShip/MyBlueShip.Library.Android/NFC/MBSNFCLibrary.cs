using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using Android.Nfc;
using Java.Lang;
using Android.Nfc.Tech;
using MyBlueShip.API.DataModel;
using MyBlueShip.Common;
using MyBlueShip.Library.Android.Design;

namespace MyBlueShip.Library.Android.NFC
{
	[Activity(Label = "MBSNFCLibraryRead")]
    public abstract class MBSNFCLibraryRead : Activity
    {
        private MBSProc_Var fct = new MBSProc_Var();
        NfcAdapter adapter;
        PendingIntent pendingIntent;
        IntentFilter[] writeTagFilters;
        Handler handler = new Handler();


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            adapter = NfcAdapter.GetDefaultAdapter(this);
            pendingIntent = PendingIntent.GetActivity(this, 0, new Intent(this,
                    Class).AddFlags(ActivityFlags.SingleTop), 0);
            IntentFilter tagDetected = new IntentFilter(NfcAdapter.ActionTagDiscovered);
            tagDetected.AddCategory(Intent.CategoryDefault);
            writeTagFilters = new IntentFilter[] { tagDetected };
        }

        protected override void OnStart()
        {
            base.OnStart();
            //handler.PostDelayed(action, 15000);
        }

        protected override void OnResume()
        {
            base.OnResume();
            if (this.adapter.IsEnabled == false)
                DesignComponent.CreateAlertDialog(this, (int)AlertDialogType.NONFC);
            this.adapter.EnableForegroundDispatch(this, pendingIntent, writeTagFilters, null);
        }


        protected override void OnPause()
        {
            base.OnPause();
            adapter.DisableForegroundDispatch(this);
        }

        protected async void NfcRead(Intent intent)
        {
            int iStart, iEnd, iStatut;
            iStatut = 2;
            string sData = "";
            // R�cup�re le tag
			Tag myTag = (Tag)intent.GetParcelableExtra(NfcAdapter.ExtraTag);
			if (myTag == null) {
				ResultReading (false, null, "Error reading tag", -1);
				return;
			}

            // Parametre partager
          /*  ISharedPreferences settings = GetSharedPreferences("MyLibNFC", 0);
            ISharedPreferencesEditor editor = settings.Edit();

            iStart = settings.GetInt("Start_block", 0);
            iEnd = settings.GetInt("End_block", 0);*/
            iStart = 0;
            iEnd = 8;
            NfcV nfcv = NfcV.Get(myTag);
            if (nfcv != null)
            {
                // send read command
                try
                {
                    nfcv.Close();
                    nfcv.Connect();
                    for (int i = iStart; i <= iEnd; i++)
                    {
                        byte[] data = nfcv.Transceive(fct.NFCV_ReadBlock(myTag.GetId(), i));
                        sData += ";" + fct.bytesToHex(data);
                        Console.WriteLine("SDATA Tag " + sData);
                    }
                    iStatut = 1;
                }
                catch (Java.Lang.Exception e)
                {
                    iStatut = 2;
                }
            }

            // Mise � jour de l'UID
           /* editor.PutBoolean("Read_end", true);
            editor.PutInt("Read_statut", iStatut);
            editor.PutString("Read_data", sData);
            editor.PutString("Read_UID", fct.bytesToHex(myTag.GetId()));
            editor.Commit();
*/
            // Arret de callback
            // handler.removeCallbacks(runnable);
            // Ferme la fenetre
            // Finish();
            try
            {
                Console.WriteLine("TAGUID :" + fct.bytesToHex(myTag.GetId()));
                MBSTags tag = await MBSApi.Instance.GetTagUID(fct.bytesToHex(myTag.GetId()));
                if (tag == null)
                    this.ResultReading(true, null, "Tag not found or no internet connection", Constants.TAG_Owner_Statuts.BELONGS_NOT_TO_MBS);
                else if (tag.id_user == 0)
                    this.ResultReading(true, tag, null, Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_NOUSER);
                else if(MBSApi.GetAppType(tag.id_app) != MBSApi.Instance.appType)
                    this.ResultReading(true, tag, null, Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_WRONGAPP);
				else if (tag.id_user == MBSApi.Instance.currentUserId)
                    this.ResultReading(true, tag, null, Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_OWN);
                else if (tag.id_user > 0)
                    this.ResultReading(true, tag, null, Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_USER);
            }
            catch (MBSExceptionApi e)
            {
                Console.WriteLine("An error occur in ReadingTag-gettaguid " + e.Message);
                this.ResultReading(false, null, "An error occur in ReadingTag-gettaguid " + e.Message, -1);
            }
        }

        protected override void OnNewIntent(Intent intent)
        {
            if (NfcAdapter.ActionTagDiscovered.Equals(intent.Action))
            {
                this.NfcRead(intent);
            }
        }
        protected abstract void ResultReading(bool readEnd, MBSTags tag, string error, int status);        
    }
}