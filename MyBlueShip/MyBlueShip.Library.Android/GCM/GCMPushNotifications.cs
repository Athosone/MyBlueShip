
using Android.App;
using Android.Content;
using Android.OS;
using Android.Gms.Gcm;
using Android.Gms.Gcm.Iid;
using Android.Preferences;
using MyBlueShip.Common;
using Android.Support.V4.App;
using MyBlueShip.Library.Android.GCM.HandleNotifications;

namespace MyBlueShip.Library.Android.GCM
{

    [Service(Exported = false), IntentFilter(new[] { "com.google.android.gms.iid.InstanceID" })]
    public class MyInstanceIDListenerService : InstanceIDListenerService
    {
        /**
         * Called if InstanceID token is updated.
         * May occur if the security of the previous token had been compromised
         * and is initiated by the InstanceID provider.
         */
        public override void OnTokenRefresh()
        {
            // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
            StartService(new Intent(this, typeof(RegistrationIntentService)));
        }
    }

    [Service(Exported = false)]
    public class RegistrationIntentService : IntentService 
	{
	    private const string tag= "MyBlueShip.Library.Android.GCM.RegistrationIntentService";
        private string SENDER_ID;
        //    private const string["RegistrationIntentService"] TOPICS = {"global"};

        public RegistrationIntentService() : base(tag) {
            switch (MBSApi.Instance.appType)
            {
                case APP_Type.NONE:
                    break;
                case APP_Type.MOBMYCAR:
				SENDER_ID = "688834807785";//"975087379370";
                    break;
                case APP_Type.MOBMYBAG:
                    break;
                default:
                    break;
            }
        }
      
        protected override void OnHandleIntent(Intent intent) {
			System.Console.WriteLine("Constructor Service, PackageName: " + this.PackageName);
    		ISharedPreferences sharedPreferences = PreferenceManager.GetDefaultSharedPreferences(this);
            string tokenSP = "NONE";
            if ((tokenSP = sharedPreferences.GetString("GCM_TOKEN", "NONE")) != "NONE")
            {
                System.Console.WriteLine("GCM Registration Token Already exist: " + tokenSP);
                return;
            }
            try
            {
                // Initially a network call, to retrieve the token, subsequent calls are local.
                ISharedPreferencesEditor editor = sharedPreferences.Edit();


                InstanceID instanceID = InstanceID.GetInstance(this);
                string token = instanceID.GetToken(SENDER_ID, GoogleCloudMessaging.InstanceIdScope, null);
                System.Console.WriteLine("GCM Registration Token: " + token);
                editor.PutString("GCM_TOKEN", token);
                MBSApi.Instance.CreatePush(token);

                // TODO: send any registration to my app's servers, if applicable.
                // e.g. sendRegistrationToServer(token);

                // TODO: Subscribe to topic channels, if applicable.
                // e.g. for (String topic : TOPICS) {
                //          GcmPubSub pubSub = GcmPubSub.getInstance(this);
                //          pubSub.subscribe(token, "/topics/" + topic, null);
                //       }

                sharedPreferences.Edit().PutBoolean("Cool", true).Apply();
            }
            catch (Java.Lang.Exception e)
            {
                System.Console.WriteLine("Failed to complete token refresh" + e.Message);
                sharedPreferences.Edit().PutBoolean("Cool", false).Apply();
            }
            catch (MBSExceptionApi e)
            {
                System.Console.WriteLine("Error While registering GCM Token on server: " + e.Message);
            }
		// Notify UI that registration has completed, so the progress indicator can be hidden.
		//LocalBroadcastManager.GetInstance(this).SendBroadcast(new Intent(getString(R.string.intent_name_REGISTRATION_COMPLETE)));
		}
	}
}
