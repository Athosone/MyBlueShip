using System;

using Android.App;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.Gms.Location;
using Android.Locations;
using Android.OS;
using MyBlueShip.Common;
using System.Timers;
using Android.Content;
using MyBlueShip.Library.Android.Design;
using Android.Provider;

namespace MyBlueShip.Library.Android.MAP
{
    [Activity(Label = "MapDataProvider")]
    public abstract class MapDataProvider : Activity, IGoogleApiClientConnectionCallbacks, IGoogleApiClientOnConnectionFailedListener, global::Android.Gms.Location.ILocationListener
    {
        IGoogleApiClient googleApiClient;

        public Location lastLocation { get; set; }
        private LocationRequest mLocationRequest;// { get; set; }
        private bool isRequesting;// { get; private set; }
        private Timer timer;


        #region Activity LifeCycle

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            lastLocation = null;
          
            googleApiClient = new GoogleApiClientBuilder(this)
            .AddConnectionCallbacks(this)
            .AddOnConnectionFailedListener(this)
            .AddApi(LocationServices.API)
            .Build();
            googleApiClient.Connect();
            mLocationRequest = new LocationRequest();
            mLocationRequest.SetInterval(Constants.MapParameters.INTERVAL);
            mLocationRequest.SetFastestInterval(Constants.MapParameters.FASTEST_INTERVAL);
            mLocationRequest.SetPriority(LocationRequest.PriorityHighAccuracy);
            updateValuesFromBundle(bundle);
        }

        private void CheckForGps()
        {
            LocationManager locationManager = (LocationManager)GetSystemService(Context.LocationService);
            if (locationManager.IsProviderEnabled(LocationManager.GpsProvider) == false)
            {
                DesignComponent.CreateAlertDialog(this, (int)AlertDialogType.NOGPS);
            }
        }

        private void updateValuesFromBundle(Bundle savedInstanceState)
        {
            if (savedInstanceState != null)
            {
                // Update the value of mRequestingLocationUpdates from the Bundle, and
                // make sure that the Start Updates and Stop Updates buttons are
                // correctly enabled or disabled.
                if (savedInstanceState.KeySet().Contains(Constants.MapParameters.REQUESTING_LOCATION_UPDATES_KEY))
                {
                    isRequesting = savedInstanceState.GetBoolean(
                            Constants.MapParameters.REQUESTING_LOCATION_UPDATES_KEY);
                }

                // Update the value of mCurrentLocation from the Bundle and update the
                // UI to show the correct latitude and longitude.
                if (savedInstanceState.KeySet().Contains(Constants.MapParameters.CURRENT_LOCATION_KEY))
                {
                    // Since LOCATION_KEY was found in the Bundle, we can be sure that
                    // mCurrentLocationis not null.
                    lastLocation = (Location)savedInstanceState.GetParcelable(Constants.MapParameters.CURRENT_LOCATION_KEY);
                    DidUpdateNewLocation(lastLocation);
                }
            }
        }

        protected override void OnResume()
        {
            base.OnResume();
            timer = new Timer();
            timer.Interval = 3000;
            timer.Elapsed += t_Elapsed;
            timer.Enabled = true;
            timer.Start();
            if (googleApiClient.IsConnected && isRequesting == false)
            {
                startLocationUpdates();
            }
        }

        private void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (lastLocation != null)
            {
                RunOnUiThread(() =>  DidUpdateNewLocation(lastLocation));
            }
        }

        protected override void OnPause()
        {
            base.OnPause();
            if (googleApiClient.IsConnected && isRequesting == true)
            {
                stopLocationUpdates();
                this.timer.Stop();
                this.timer = null;
            }
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            outState.PutBoolean(Constants.MapParameters.REQUESTING_LOCATION_UPDATES_KEY, this.isRequesting);
            outState.PutParcelable(Constants.MapParameters.CURRENT_LOCATION_KEY, this.lastLocation);
            base.OnSaveInstanceState(outState);
        }

        #endregion

        #region IGoogleApiClient Implementation

        public void OnConnected(Bundle bundle)
        {
            Console.WriteLine("MapDataProvider: OnConnected");
            lastLocation = LocationServices.FusedLocationApi.GetLastLocation(googleApiClient);
            if (lastLocation != null)
            {
                DidUpdateNewLocation(lastLocation);
            }
        }

        public void OnLocationChanged(Location location)
        {
            if (location != null)
            {
                Console.WriteLine("MapDataProvider: LocationChanged");
                this.lastLocation = location;
                DidUpdateNewLocation(location);
            }
        }


        public void OnDisconnected(Bundle bundle)
        {
        }

        public void OnConnectionSuspended(int cause)
        {
        }

        public void OnConnectionFailed(ConnectionResult result)
        {
            DidReceivedError("MapDataProvider Failed to connect " + result.ErrorCode + " " + result.DescribeContents());
        }

        #endregion

        protected void startLocationUpdates()
        {
            if (googleApiClient.IsConnected)
            {
                Console.WriteLine("MapDataProvider: StartLocationUpdates");
                LocationServices.FusedLocationApi.RequestLocationUpdates(
                    googleApiClient, mLocationRequest, this);
                this.isRequesting = true;
            }
        }

        protected void stopLocationUpdates()
        {
            if (googleApiClient.IsConnected)
            {
                Console.WriteLine("MapDataProvider: StopLocationUpdates");
                LocationServices.FusedLocationApi.RemoveLocationUpdates(
                    googleApiClient, this);
                this.isRequesting = false;
            }
        }

        protected abstract void DidUpdateNewLocation(Location location);
        protected abstract void DidReceivedError(string error);

    }
}