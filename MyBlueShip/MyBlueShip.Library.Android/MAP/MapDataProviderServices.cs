﻿using System;
using Android.App;
using Android.Content;
using Android.Gms.Gcm.Iid;
using MyBlueShip.Common;
using Android.Gms.Common.Apis;
using Android.OS;
using Android.Gms.Location;
using Android.Locations;
using Android.Gms.Common;
using MyBlueShip.Library.Android.Design;
using Android.Provider;
using Android.App;
using MyBlueShip.Library.Android.Events;

namespace MyBlueShip.Library.Android
{

	public class LocationServiceBinder : Binder
	{
		public MapDataProviderServices Service {
			get { return this.service; }
		}

		protected MapDataProviderServices service;

		public bool IsBound { get; set; }

		public LocationServiceBinder (MapDataProviderServices service)
		{
			this.service = service;
		}
	}

	public class LocationServiceConnection : Java.Lang.Object, IServiceConnection
	{
		LocationServiceBinder binder;
		public event EventHandler<ServiceConnectedEventArgs> ServiceConnected = delegate {};

		public LocationServiceConnection (LocationServiceBinder binder)
		{
			if (binder != null) {
				this.binder = binder;
			}
		}

		public void OnServiceConnected (ComponentName name, IBinder service)
		{
			LocationServiceBinder serviceBinder = service as 
				LocationServiceBinder;

			if (serviceBinder != null) {
				this.binder = serviceBinder;
				this.binder.IsBound = true;
				// raise the service bound event
				this.ServiceConnected(this, new 
					ServiceConnectedEventArgs () { Binder = service } );

				// begin updating the location in the Service
				serviceBinder.Service.StartLocationUpdates();
			}
		}

		public void OnServiceDisconnected (ComponentName name) { this.binder.IsBound = false; }
	}

	[Service (Exported = false)]
	public class MapDataProviderServices : Service, IGoogleApiClientConnectionCallbacks, IGoogleApiClientOnConnectionFailedListener, global::Android.Gms.Location.ILocationListener
	{
		private const string tag = "MyBlueShip.Library.Android.MapDataProviderServices";
		IBinder binder;

		#region Location related vars

		IGoogleApiClient googleApiClient;
		LocationRequest mLocationRequest;
		bool isRequesting;
		public Location lastLocation { get; set; }
		#endregion

		public event EventHandler<LocationChangedEventArgs> LocationChanged = delegate { };
        public event EventHandler<StatusConnectivityEventArgs> GPSDisabled = delegate { };

        public override IBinder OnBind (Intent intent)
		{
			binder = new LocationServiceBinder (this);
			return binder;
		}

		public override void OnDestroy ()
		{

			stopLocationsUpdates ();
		}


		public override StartCommandResult OnStartCommand (Intent intent, StartCommandFlags flags, int startId)
		{
			return StartCommandResult.Sticky;
		}

		public void StartLocationUpdates ()
		{
            if (this.googleApiClient == null) {
				googleApiClient = new GoogleApiClientBuilder (this)
				.AddConnectionCallbacks (this)
				.AddOnConnectionFailedListener (this)
				.AddApi (LocationServices.API)
				.Build ();
				googleApiClient.Connect ();
				mLocationRequest = new LocationRequest ();
				mLocationRequest.SetInterval (Constants.MapParameters.INTERVAL);
				mLocationRequest.SetFastestInterval (Constants.MapParameters.FASTEST_INTERVAL);
				mLocationRequest.SetPriority (LocationRequest.PriorityHighAccuracy);
			} 
		}
			
		private void stopLocationsUpdates ()
		{
			if (googleApiClient.IsConnected) {
				Console.WriteLine ("MapDataProviderSERVICES: StopLocationUpdates");
				LocationServices.FusedLocationApi.RemoveLocationUpdates (
					googleApiClient, this);
				this.isRequesting = false;
			}
		}

		#region IGoogleApiClient Implementation

		public void OnConnected(Bundle bundle)
		{
			Console.WriteLine("MapDataProvider: OnConnected");
            LocationManager locationManager = (LocationManager)GetSystemService(Context.LocationService);
            if (locationManager.IsProviderEnabled(LocationManager.GpsProvider) == false)
            {
                this.GPSDisabled(this, new StatusConnectivityEventArgs(StatusConnectivityType.NOGPS));
            }
            else
            {
                LocationServices.FusedLocationApi.RequestLocationUpdates(
                                googleApiClient, mLocationRequest, this);
                this.isRequesting = true;
                lastLocation = LocationServices.FusedLocationApi.GetLastLocation(googleApiClient);
            }
       	}

		public void OnLocationChanged(Location location)
		{
			if (location != null)
			{
				Console.WriteLine("MapDataProvider: LocationChanged");
				this.lastLocation = location;
				this.LocationChanged (this, new LocationChangedEventArgs (location));
			}
        }

		public void OnDisconnected(Bundle bundle)
		{
		}

		public void OnConnectionSuspended(int cause)
		{
		}

		public void OnConnectionFailed(ConnectionResult result)
		{
			Console.WriteLine ("Failed to connect to googleapiclient: " + result);
		}

		#endregion

	}
}

