using System;
using Android.App;
using Android.Content;
using Android.Gms.Gcm.Iid;
using MyBlueShip.Common;
using Android.Gms.Common.Apis;
using Android.OS;
using Android.Gms.Location;
using Android.Locations;

namespace MyBlueShip.Library.Android
{

	public class ServiceConnectedEventArgs : EventArgs
	{
		public 	IBinder Binder { get; set; }

		public ServiceConnectedEventArgs()
		{
		}

		public ServiceConnectedEventArgs(IBinder binder)
		{
			this.Binder = binder;
		}
	}
}

