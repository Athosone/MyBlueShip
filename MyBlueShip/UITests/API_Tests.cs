﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest;

namespace MyBlueShip.UITests
{
    [TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
    class API_Tests
    {
        IApp app;
        Platform platform;

        public API_Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
            MBSApi.Instance.setInfosApp(APP_Type.MOBMYCAR, Platform_APP.Android);
        }
    }
}
