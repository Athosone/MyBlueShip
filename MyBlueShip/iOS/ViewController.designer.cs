// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MyBlueShip.iOS
{
	[Register ("MainPageViewController")]
	partial class MainPageViewController
	{
		[Outlet]
		UIKit.UIButton Button { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lectureQrCode { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel MobMyCommunity { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel MobMyParking { get; set; }

		[Action ("UIButton8_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void UIButton8_TouchUpInside (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (lectureQrCode != null) {
				lectureQrCode.Dispose ();
				lectureQrCode = null;
			}
			if (MobMyCommunity != null) {
				MobMyCommunity.Dispose ();
				MobMyCommunity = null;
			}
			if (MobMyParking != null) {
				MobMyParking.Dispose ();
				MobMyParking = null;
			}
		}
	}
}
