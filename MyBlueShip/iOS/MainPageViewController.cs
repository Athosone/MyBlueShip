﻿using System;	
using UIKit;
using Foundation;

namespace MyBlueShip.iOS
{
	public partial class MainPageViewController : UIViewController
	{

		public MainPageViewController (IntPtr handle) : base (handle)
		{		
		}


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Code to start the Xamarin Test Cloud Agent
			#if ENABLE_TEST_CLOUD
			Xamarin.Calabash.Start ();
			#endif

			//set the constants
			this.lectureQrCode.Text = Constants.StringApplication.LECTURE_QRCODE_OR_MOB;
			this.MobMyCommunity.Text = Constants.StringApplication.MY_MOB_COMMUNITY;
			this.MobMyParking.Text = Constants.StringApplication.MY_MOB_PARKING;

		}

		public override void DidReceiveMemoryWarning ()
		{		
			base.DidReceiveMemoryWarning ();		
			// Release any cached data, images, etc that aren't in use.		
		}

		partial void UIButton8_TouchUpInside (UIButton sender)
		{
			//PerformSegue ("detailSegue", this);
			//this.PerformSegue("goToLectureMob", this);
		}

		/*public override void PrepareForSegue (UIStoryboardSegue segue, NSObject sender)
		{
			if (segue.Identifier == "detailSegue") {

				LectureQrCodeViewController = segue.DestinationViewController;

				// do your initialisation here

			}
		}

		/*preparforsegue 
		{
			if UIStoryboardSegue.NSIdentifier == "toNextView"
			{
				destvc = UIStoryboardSegue.destVC
					destvc.jesuisunepropriete = "ttoto";	
				destvc.arrayofobject = Array;
			}
		}*/

	}
}
