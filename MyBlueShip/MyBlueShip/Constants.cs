﻿using System;

namespace MyBlueShip
{
	
	public static class Constants
	{
		public struct UIDesign
		{
			public const float DEFAULT_CORNER_RADIUS = 6.0f;
		}

		public struct ServerInfos
		{
            public const string SERVER_ADDRESS_API = "http://vps182169.ovh.net:81/api";
            public const string SERVER_ADDRESS_API_LOCAL = "http://localhost:12346/api";

            public const string SERVER_API_KEY = "j79pvA9teqCF0L11L2v6BP6A5ECKw865";
            public const string SERVER_SIGNATURE_iOS = "APP_iOS";
            public const string SERVER_SIGNATURE_Android = "APP_Android";

        }

		public struct TypefaceInfos
		{
			public const String MY_BLUE_SHIP_TYPEFACE = "GothamRnd-Bold.otf";

		}

		public struct LocationInfos
		{
			public const float CURRENT_LATITUDE = 0;
			public const float CURRENT_LONGITUDE = 0;
			public const int   ZOOM_CAMERA = 16;
		}

        public struct StringApplication
		{
			#region SHAREDPREFS TAG
			public const string TAGS_PREF = "TAGS_PREFS";
			#endregion

			#region SHAREDPREFS TAG
			public const string CURRENT_USER_ID = "CURRENT_USER_ID";
			#endregion

			#region StringTitle
			public const String APPLICATION_TITLE = "MobMyCar";
			#endregion

			#region StringLogin
			public const String LOGIN_PRESENTATION = "Se Connecter / Créer un compte";
			public const String LOGIN_TITLE = "Connexion";
			public const String LOGIN_USER = "Identifiant";
			public const String PASSWORD_USER = "Mot de passe";
			public const String AUTHENTIFICATION = "Valider";
			public const String CLOSE_APPLICATION_TITLE = "Quitter l'application ?";
			public const String CLOSE_APPLICATION_OK = "Ok";
			public const String CLOSE_APPLICATION_CANCEL = "Annuler";
			#endregion

			#region StringMainPage
			public const String LECTURE_QRCODE_OR_MOB = "Réveiller\nmon MOB";
			public const String MY_MOB_COMMUNITY = "Trouver\nune place";
			public const String MY_MOB_PARKING = "Perdu ?";
			public const String LOGOUT_TITLE = "Déconnexion ?";
			public const String LOGOUT_OK = "Ok";
			public const String LOGOUT_CANCEL = "Annuler";
			#endregion

			#region StringMyMobs
			public const String MY_MOBS = "Mes MOB";
			public const String ADD_A_MOB = "Ajouter";
			#endregion

			#region StringQrCodeOrMob
			public const String LECTURE_QRCODE = "Lecture QrCode";
			public const String LECTURE_MOB = "Lecture Mob";
			#endregion

			#region StringLectureMob
			public const String LECTURE_MY_MOB_TITLE = "Lecture MOB"; 
			public const String LECTURE_MY_MOB_CONTENT = "Passer\nvotre smartphone\nsur votre MOB.";
			public const String LECTURE_MOB_TIERS = "MOB déjà utilisé par un tiers";
			public const String LECTURE_MOB_FOR_ME = "MOB non enregistré";
			public const String LECTURE_MOB_ALREADY_FOR_ME = " a été geolocalisé avec succès!";
			#endregion

			#region StringAddMyCar
			public const String ADD_MY_CAR_TITLE = "Enregistrement my car";
			public const String MY_PSEUDO = "Pseudo";
			public const String MY_IMMATRICULATION = "N° immatriculation";
			public const String MY_COORDONNEES = "Coordonnées SIV";
			public const String MY_COLOR = "Couleur";
			public const String CHOOSE_MY_MODELE_CAR = "Quel modèle ?";
			public const String CHOOSE_MY_CAR = "Identification";
			public const String ADD_MY_CAR_BUTTON = "Enregistrer";
			public const String ADD_CAR_SUCCESS = "Votre véhicule est enregistré";
			#endregion

			#region StringMyHorodateurOrEmplacement
			public const String MY_HORODATEUR = "A payer !";
			public const String MY_EMPLACEMENT = "Scanner votre MOB pour enregistrer votre emplacement";
			#endregion

			#region StringHorodateur
			public const String HORODATEUR_INFO_HOUR = "Tarif: 1€/h - Durée max : 1h30";
			public const String HORODATEUR_INFO_PRICE = "0h00 = 0€"; 
			public const String HORODATEUR_0_MIN = "0 min";
			public const String HORODATEUR_1_H = "1 h";
			public const String HORODATEUR_2_H = "|\n45 min";
			public const String HORODATEUR_3_H = "3 h";
			public const String HORODATEUR_4_H = "1h30 max";
		    public const String HORODATEUR_VALIDATE = "Payer";
			public const String HORODATEUR_CAR_EMPTY = "Aucune voiture liée à l'utilisateur";
			public const String HORODATEUR_NEW_PARK = "Votre place a été payée";
			public const String HORODATEUR_LOADING = "En attente";
			public const String HORODATEUR_SCAN_MOB = "Scanner votre MOB pour définir votre zone tarifaire";
			public const String HORODATEUR_CANCEL = "Finir le paiement";
			public const String HORODATEUR_ADD = "Rajouter du temps ?";
			public const String HORODATEUR_CANCEL_PAYEMENT = "Votre paiement est terminé";
			#endregion

			#region StringDialogAsk
			public const String DIALOG_ASK_COMMUNICATION = "Communiquer avec le propriétaire :";
			public const String DIALOG_ASK_IMMINENT_GO = "Vous partez dans combien de temps ?";
			public const String DIALOG_ASK_PARK_PROBLEM = "Votre stationnement pose un problème.";
			public const String DIALOG_ASK_CAR_PROBLEM = "Vous avez un problème sur votre voiture.";
			public const String DIALOG_ASK_SEND_MESSAGE = "Votre message a bien été envoyé";
			#endregion

			#region StringError
			public const String ERROR_LOGIN_EMPTY = "Login vide";
			public const String ERROR_PASSWORD_EMPTY = "Password vide";
			public const String ERROR_LOGIN_WRONG = "Mauvais login";
			public const String ERROR_PSEUDO_EMPTY = "Pseudo vide";
			public const String ERROR_IMMATRICULATION_EMPTY = "Immatriculation vide";
			public const String ERROR_COORDONNEES_SIV_EMPTY = "Coordonnées vide";
			public const String ERROR_COLOR_EMPTY = "Couleur vide";
			public const String ERROR_MOB_NOT_FROM_MY_BLUE_SHIP = "Ce n'est pas un mob de My Blue Ship";
			public const String ERROR_MOB_NOT_FOR_MOB_MY_CAR = "Ce n'est pas un mob MOBmyCAR";
			public const String ERROR_HORODATEUR_EMPTY = "Aucun temps rajouté";
			public const String ERROR_HORODATEUR_WRONG = "Impossible de faire l'horodatage";
			public const String ERROR_NOT_CURRENT_TAG = "Aucun mob lié à l'utilisateur";
			public const String ERROR_CURRENT_USER_NOT_LOGGED = "Utilisateur non connecté";
			public const String ERROR_NEED_CAR = "Pouvez-vous taguer votre MOB";
			public const String ERROR_TAG_OWN_BUT_NOT_CURRENT = "Ce mob n'est pas votre MOB actuel";
			#endregion

		}

        public struct ProjectType
        {
            public const string API = "[MBSApi]";
            public const string LIBRARY_Android = "[Library_Android]";
            public const string LIBRARY_iOS = "[Library_iOS]";
            public const string MOBMYCAR = "1";
        }

        public struct ErrorAPIMessages
        {
            public const string ERROR = "[ERROR]" + ProjectType.API;
            public const string WARNING = "[WARNING]" + ProjectType.API;
            public const string ERROR_BASIC_VERIF = ERROR + "Basic verification failed, don't forget to call MBSApi.Instance.setInfos(string pseudo, string password)";
            public const string ERROR_LOGIN_PARAMETERS = ERROR + "Invalid parameters for login method";
            public const string ERROR_MBSUSER_FROMJSON = ERROR + "Can't create MBSUser from json";
            public const string ERROR_MBSTAGS_FROMJSON = ERROR + "Can't create MBSTags from json";
            public const string ERROR_MBSParking_FROMJSON = ERROR + "Can't creat MBSParking from json";

        }

        public struct MapParameters
        {
            public const int INTERVAL = 10000;
            public const int FASTEST_INTERVAL = 5000;
            public const string REQUESTING_LOCATION_UPDATES_KEY = "REQUESTING_LOCATION_UPDATES";
            public const string CURRENT_LOCATION_KEY = "CURRENT_LOCATION";
        }

        public struct TAG_Owner_Statuts
        {
            public const int BELONGS_TO_MBS_USER = 0;
            public const int BELONGS_TO_MBS_NOUSER = 1;
            public const int BELONGS_TO_MBS_WRONGAPP = 2;
            public const int BELONGS_TO_MBS_OWN = 3;
            public const int BELONGS_NOT_TO_MBS = 4;
        }

        public struct PushNotfication
        {
            public const string SENDER_ID = "97508737937";
            public const string TAG_REG_ID_Android = "RegistrationIdGCM";
            public const string PROPERTY_APP_VERSION = "PROPERTY_APP_VERSION";
			#region Receivers Notif
			public const string INTENT_FILTER_PARKING_RESPONSE = "PARKING_RESPONSE";
			#endregion
        }


    }
}

