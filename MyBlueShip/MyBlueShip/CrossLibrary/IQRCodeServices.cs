﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBlueShip.CrossLibrary
{
    public interface IQRCodeServices<T>
    {
        Task<string> ReadQRCode();
        T WriteQRCode(string text, int width, int height);
    }
}
