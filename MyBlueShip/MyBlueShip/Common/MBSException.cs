﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBlueShip.Common
{

    public enum ExceptionType
    {
        //API
        INVALIDCREDENTIALS,
        BASICVERIF_FAILED,
        FAILTOPARSERESPONSE,
        INVALIDPARAMETERS,
        SERVER_ERROR,
        //MAP
        MAP_SERVICELOCATION_DISABLED,
        //NFC
        NFC_ERRORREADTAG,
        UNKNOWN
    }

    public class MBSExceptionApi : Exception
    {
        public ExceptionType type { get; set; }

        public MBSExceptionApi()
        {
        }

        public MBSExceptionApi(string message)
        : base(message)
        {
        }

        public MBSExceptionApi(string message, ExceptionType type)
       : base(message)
        {
            this.type = type;
        }

        public MBSExceptionApi(string message, Exception inner)
        : base(message, inner)
        {
        }


    }
}
