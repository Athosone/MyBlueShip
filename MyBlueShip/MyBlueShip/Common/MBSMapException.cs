﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBlueShip.Common
{
    public class MBSMapException : MBSExceptionApi
    {
        public MBSMapException(string message, ExceptionType type) : base(message, type)
        {
        }
    }
}
