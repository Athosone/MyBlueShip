﻿using System;

namespace MyBlueShip
{
	
	public static class Constants
	{
		public struct UIDesign
		{
			public const float DEFAULT_CORNER_RADIUS = 6.0f;
		}

		public struct ServerInfos
		{
		}

		public struct StringApplication
		{
			#region StringTitle
			public const String APPLICATION_TITLE = "MyBlueShip";
			#endregion

			#region StringMainPage
			public const String LECTURE_QRCODE_OR_MOB = "Lecture Mob ou QrCode";
			public const String MY_MOB_COMMUNITY = "My Mob Community";
			public const String MY_MOB_PARKING = "My Mob Parking";
			#endregion

			#region StringQrCodeOrMob
			public const String LECTURE_QRCODE = "Lecture QrCode";
			public const String LECTURE_MOB = "Lecture Mob";
			#endregion

			#region StringFooter
			public const String FOOTER_1 = "My BLUE SHIP";
			public const String FOOTER_2 = "Smart Home & Interactive People";
			#endregion

		}
	}
}

