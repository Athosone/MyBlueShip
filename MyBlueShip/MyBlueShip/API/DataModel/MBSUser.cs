﻿using MyBlueShip.API.JSONConverter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBlueShip
{
    public class MBSUser
    {
        public ulong id { get; set; }
     

        private short id_state;

        public short ID_State
        {
            get { return id_state; }
            set { id_state = value; }
        }

        private DateTime datetime;

        public DateTime DateTime
        {
            get { return datetime; }
            set { datetime = value; }
        }

        private int salt;

        public int Salt
        {
            get { return salt; }
            set { salt = value; }
        }
        private string pseudo;

        public string Pseudo
        {
            get { return pseudo; }
            set { pseudo = value; }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private string fbuid;

        public string FBUid
        {
            get { return fbuid; }
            set { fbuid = value; }
        }

        private string id_file_avatar;

        public string ID_File_Avatar
        {
            get { return id_file_avatar; }
            set { id_file_avatar = value; }
        }

        private bool isCommunityVisible;

        [JsonConverter(typeof(BoolConverter))]
        public bool IsCommunityVisible
        {
            get { return isCommunityVisible; }
            set { isCommunityVisible = value; }
        }

        private bool isAcceptMessagesFromThirds;

        [JsonConverter(typeof(BoolConverter))]
        public bool IsAcceptMessagesFromThirds
        {
            get { return isAcceptMessagesFromThirds; }
            set { isAcceptMessagesFromThirds = value; }
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime Birthday { get; set; }

        public string Gender { get; set; }

        public string Phone { get; set; }

        public string CP { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string Address { get; set; }

    }
}