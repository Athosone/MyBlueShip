﻿using MyBlueShip.API.JSONConverter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBlueShip.API.DataModel
{
    public class MBSParking
    {

        public MBSParking() { }

        public ulong id { get; set; }

        public ulong id_tag { get; set; }

        [JsonConverter(typeof(BoolConverter))]
        public bool id_state { get; set; }

        public DateTime DateCreation { get; set; }

        [JsonConverter(typeof(BoolConverter))]
        public bool InParking { get; set; }

        public float Longitude { get; set; }

        public float Latitude { get; set; }

        public DateTime? EndParking { get; set; }

        [JsonConverter(typeof(BoolConverter))]
        public bool Warning { get; set; }

        public string SIV { get; set; }

        public int? id_icone { get; set; }

        public ulong id_user { get; set; }

    }
}
