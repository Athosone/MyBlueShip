﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBlueShip.API.DataModel
{
    //Nom du test :	3- getAllTagsForCurrentUser (orderedtest1)
    //  Sortie du test :	Réussite
    // StandardOutput de résultat :	
    /*Result getalltags: {"perfms":"0","result":[
    {"id":"11","id_app":"1","AppName":"Mob My Car","id_user":"23","DateCreation":"2015-08-31 14:02:11.8070000","Name":"PEUGEOT 405","TagUniqueId":"SuperTagUniqueId",
    "id_file":null,"imagesalt":null,"imagestate":null,"imagecontenttype":null},
    {"id":"10","id_app":"1","AppName":"Mob My Car","id_user":"23","DateCreation":"2015-08-31 13:59:12.5000000","Name":"PEUGEOT","TagUniqueId":"Coucou","id_file":"70","imagesalt":"88613319","imagestate":"1","imagecontenttype":"image\/jpeg"},{"id":"8","id_app":"1","AppName":"Mob My Car","id_user":"23","DateCreation":"2015-08-14 17:39:22.5730000","Name":"mon avion","TagUniqueId":"cdsvdf,nk","id_file":"37","imagesalt":"97758692","imagestate":"1","imagecontenttype":"image\/png"},{"id":"7","id_app":"1","AppName":"Mob My Car","id_user":"23","DateCreation":"2015-08-14 17:39:11.5770000","Name":"Tag 4","TagUniqueId":"465vf4df","id_file":null,"imagesalt":null,"imagestate":null,"imagecontenttype":null},{"id":"6","id_app":"1","AppName":"Mob My Car","id_user":"23","DateCreation":"2015-08-14 17:39:02.6370000","Name":"Tag 3","TagUniqueId":"dcvfd4","id_file":"57","imagesalt":"99026884","imagestate":"1","imagecontenttype":"image\/png"},{"id":"5","id_app":"1","AppName":"Mob My Car","id_user":"23","DateCreation":"2015-08-14 17:38:54.5430000","Name":"Tag 2","TagUniqueId":"12345687","id_file":"53","imagesalt":"14156701","imagestate":"1","imagecontenttype":"image\/png"},{"id":"1","id_app":"1","AppName":"Mob My Car","id_user":"23","DateCreation":"2015-08-14 17:38:19.6900000","Name":"Tag 1","TagUniqueId":"123456","id_file":"55","imagesalt":"24307841","imagestate":"1","imagecontenttype":"image\/png"}]}
    */
    //http://vps182169.ovh.net:81/api/tags/1/23


    public class MBSTags
    {
        public MBSTags() { }

        public ulong id { get; set; }

        public string id_app { get; set; }

        public ulong id_user { get; set; }

        public string id_state { get; set; }

        public DateTime DateCreation { get; set; }

        public string Name { get; set; }

        public string TagUniqueId { get; set; }

        public string imagesalt { get; set; }

        public string imagecontenttype { get; set; }

        public string id_file { get; set; }

        public MBSParking parking { get; set; }

       // public MBSBag bag { get; set; }
    }
}
