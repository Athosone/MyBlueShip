﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MyBlueShip.Common;
using Newtonsoft.Json;
using System.Diagnostics;
using MyBlueShip.API.DataModel;

namespace MyBlueShip
{

    public enum APP_Type
    {
        NONE,
        MOBMYCAR = 1,
        MOBMYBAG
    }

    public enum Platform_APP
    {
        NONE,
        iOS,
        Android
    }

     public sealed class MBSApi
    {
        public Platform_APP platform { get; set; }
        public APP_Type appType { get; set; }
        public HttpClient client;
		public ulong currentUserId = 0;
        public string RegistrationId;

        #region Singleton
        private static volatile MBSApi _instance = null;
        private static object syncRoot = new Object();
        private MBSApi() { }
        public static MBSApi Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (_instance == null)
                            _instance = new MBSApi();
                    }
                    _instance.client = new HttpClient();
                    _instance.client.BaseAddress = new Uri(Constants.ServerInfos.SERVER_ADDRESS_API);//Constants.ServerInfos.SERVER_ADDRESS_API);
                    _instance.client.DefaultRequestHeaders.Add("MBS_API_KEY", Constants.ServerInfos.SERVER_API_KEY);
                    _instance.client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    _instance.appType = APP_Type.NONE;
                    _instance.platform = Platform_APP.NONE;
                }
                return _instance;
            }
        }
        #endregion

        public static APP_Type GetAppType(string id_app)
        {
            switch (id_app)
            {
                case "Mob My Car":
                    return APP_Type.MOBMYCAR;
                case "1":
                    return APP_Type.MOBMYCAR;
                default:
                    return APP_Type.NONE;
            }
        }

       public void setInfosApp(APP_Type typeApp, Platform_APP platform_app)
        {
            Instance.appType = typeApp;
            Instance.platform = platform_app;
            if (platform == Platform_APP.iOS)
            {
                switch (typeApp)
                {
                    case APP_Type.NONE:
                        break;
                    case APP_Type.MOBMYCAR:
                        _instance.client.DefaultRequestHeaders.Add("SIGNATURE", Constants.ServerInfos.SERVER_SIGNATURE_iOS + "MOBMYCAR");
                        break;
                    case APP_Type.MOBMYBAG:
                        _instance.client.DefaultRequestHeaders.Add("SIGNATURE", Constants.ServerInfos.SERVER_SIGNATURE_iOS + "MOBMYBAG");
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (typeApp)
                {
                    case APP_Type.NONE:
                        break;
                    case APP_Type.MOBMYCAR:
                        _instance.client.DefaultRequestHeaders.Add("SIGNATURE", Constants.ServerInfos.SERVER_SIGNATURE_Android + "MOBMYCAR");
                        break;
                    case APP_Type.MOBMYBAG:
                        _instance.client.DefaultRequestHeaders.Add("SIGNATURE", Constants.ServerInfos.SERVER_SIGNATURE_Android + "MOBMYBAG");
                        break;
                    default:
                        break;
                }
            }

        }
       
        private bool CheckBasicVerification()
        {
            if (Instance.appType == APP_Type.NONE || Instance.platform == Platform_APP.NONE)
                return false;

            return true;
        }

        private async Task<string> GetRequest(string endPoint)
        {
            HttpResponseMessage response = await client.GetAsync(client.BaseAddress + endPoint);
            string result = await response.Content.ReadAsStringAsync();
            return result;
        }

        private async Task<string> PostRequest(JObject postData, string endPoint)
        {
            HttpContent content = new StringContent(postData.ToString());
            HttpResponseMessage response = await Instance.client.PostAsync(Instance.client.BaseAddress + endPoint, content);
            string result = await response.Content.ReadAsStringAsync();
            return result;
        }

        public async Task<string> getApps()
        {
            string result = await GetRequest("/apps/");
            Debug.WriteLine(result);
            return result;
        }

        #region User

        public async Task<MBSUser> login(string pseudo, string passwordSha1)
        {
            if (CheckBasicVerification() == false)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_BASIC_VERIF, ExceptionType.BASICVERIF_FAILED);
            if (pseudo.Length == 0 || passwordSha1.Length == 0)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + "Invalid Parameters", ExceptionType.INVALIDPARAMETERS);
            JObject postData = new JObject();
            postData["pseudoOrEmail"] = pseudo;
            postData["PasswordSHA1"] = passwordSha1;
            string result = await PostRequest(postData, "/users/login");
            Debug.WriteLine("Result Login : " + result);
            JObject resultJson = JObject.Parse(result);
            if (resultJson["error"] != null)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + resultJson["error"].ToString(), ExceptionType.UNKNOWN);
            if (resultJson["result"].ToString() == null || resultJson["result"].ToString().Length == 0)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + "Invalid credentials", ExceptionType.INVALIDCREDENTIALS);
            Debug.WriteLine("ooooo" + resultJson["result"].ToString());
            string resultUser = await GetRequest("/users/" + resultJson["result"]["idUser"]);
            try
            {
                Debug.WriteLine("User:" + resultUser);
                JObject userInfos = JObject.Parse(resultUser);
                MBSUser user = JsonConvert.DeserializeObject<MBSUser>(userInfos["result"].ToString());
				this.currentUserId = user.id;
                return user;
            }
            catch (Exception e)
            {
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_MBSUSER_FROMJSON + e.Message, ExceptionType.FAILTOPARSERESPONSE);
            }
        }

        public async Task<MBSUser> GetUser(ulong id)
        {
            if (CheckBasicVerification() == false)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_BASIC_VERIF, ExceptionType.BASICVERIF_FAILED);
            if (id == 0)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + "Invalid Parameters", ExceptionType.INVALIDPARAMETERS);
            string result = await GetRequest("/users/" + id);
            JObject resJson = JObject.Parse(result);

            if (resJson["error"] != null)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + resJson["error"].ToString(), ExceptionType.UNKNOWN);
            try
            {
                MBSUser user = JsonConvert.DeserializeObject<MBSUser>(resJson["result"].ToString());

                return user;
            }
            catch (Exception e)
            {
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_MBSTAGS_FROMJSON + e.Message, ExceptionType.FAILTOPARSERESPONSE);
            }
        }

        #endregion

            #region MBSParking


        public async Task<List<MBSParking>> GetCommnityUsers(double latitude, double longitude, double radiusMeter)
        {
            if (CheckBasicVerification() == false)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_BASIC_VERIF, ExceptionType.BASICVERIF_FAILED);
            string endPoint = "/parkings/community/" + this.currentUserId + "/" + latitude + "/" + longitude + "/" + radiusMeter;
            endPoint = endPoint.Replace(',', '.');
            Debug.WriteLine("EndPoint GetCommunityUsers: " + endPoint);

            string result = await this.GetRequest(endPoint);
            Debug.WriteLine("Result GetCommunityUsers: " + result);
            JObject resJson = JObject.Parse(result);
            if (resJson["error"] != null)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + resJson["error"].ToString(), ExceptionType.UNKNOWN);
            return JsonConvert.DeserializeObject<List<MBSParking>>(resJson["result"].ToString());
        }

        /// <summary>
        /// 
        /// Update or create a new Parking for an associated tag
        /// Warning: Not tested Yet
        /// 
        /// </summary>
        /// <param name="datas">List<KeyValuePair<String, String>></param>
        /// <param name="tagid">Tag Id</param>
        /// <returns></returns>
        public async Task<bool> UpdateParking(List<KeyValuePair<String, String>> datas, ulong tagid)
        {
            if (CheckBasicVerification() == false)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_BASIC_VERIF, ExceptionType.BASICVERIF_FAILED);
            if (datas == null || datas.Count != 7 )
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + "Invalid Parameters", ExceptionType.INVALIDPARAMETERS);
            JObject postData = new JObject();
            foreach (KeyValuePair<String, String> kvp in datas)
            {
                postData[kvp.Key] = kvp.Value;                
            }
            string result = await this.PostRequest(postData, "/parkings/update/" + tagid);
            JObject resJson = JObject.Parse(result);
            if (resJson["error"] != null)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + resJson["error"].ToString(), ExceptionType.UNKNOWN);
            return true;
        }

        public async Task<MBSParking> GetMMCParking(ulong tagId)
        {
            if (CheckBasicVerification() == false)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_BASIC_VERIF, ExceptionType.BASICVERIF_FAILED);
            string result = await GetRequest("/parkings/" + tagId);
            JObject resJson = JObject.Parse(result);
            if (resJson["error"] != null)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + resJson["error"].ToString(), ExceptionType.UNKNOWN);
            try
            {
                Debug.WriteLine("MBSPark: " + result);
                MBSParking park = JsonConvert.DeserializeObject<MBSParking>(resJson["result"].ToString());
                if (park != null)
                    return park;
                else
                    return null;
            }
            catch (Exception e)
            {
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_MBSParking_FROMJSON + e.Message, ExceptionType.FAILTOPARSERESPONSE);
            }
        }
        #endregion
        #region Tags

        public async Task<List<MBSTags>> GetAllTags(ulong userId)
        {
            if (CheckBasicVerification() == false)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_BASIC_VERIF, ExceptionType.BASICVERIF_FAILED);
            ulong uId = 0;
            if (userId == 0)
                uId = this.currentUserId;
            else
                uId = userId;
            Debug.WriteLine(this.client.BaseAddress + "/tags/" + (int)this.appType + "/" + uId);
            string result = await GetRequest("/tags/" + (int)this.appType + "/" + uId);
            JObject resJson = JObject.Parse(result);
            if (resJson["error"] != null)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + resJson["error"].ToString(), ExceptionType.UNKNOWN);
            try
            {
                List<MBSTags> tags = JsonConvert.DeserializeObject<List<MBSTags>>(resJson["result"].ToString());
                if (tags == null)
                    return null;
                foreach (MBSTags tag in tags)
                {
                   var res = await GetMMCParking(tag.id);
                   if (res != null)
                      tag.parking = res;
                }

                return tags;
            }
            catch (Exception e)
            {
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_MBSTAGS_FROMJSON + e.Message, ExceptionType.FAILTOPARSERESPONSE);
            }
        }


        public async Task<MBSTags> GetTag(ulong tagId)
        {
            if (CheckBasicVerification() == false)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_BASIC_VERIF, ExceptionType.BASICVERIF_FAILED);
            if (tagId == 0)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + "Invalid Parameters", ExceptionType.INVALIDPARAMETERS);
            Debug.WriteLine(this.client.BaseAddress + "/tags/" + tagId);
            string result = await GetRequest("/tags/" + tagId);
            JObject resJson = JObject.Parse(result);

            if (resJson["error"] != null)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + resJson["error"].ToString(), ExceptionType.UNKNOWN);
            try
            {
                MBSTags tag = JsonConvert.DeserializeObject<MBSTags>(resJson["result"].ToString());
                if (tag == null)
                    return null;
                var res = await GetMMCParking(tag.id);
                if (res != null)
                    tag.parking = res;
                return tag;
            }
            catch (Exception e)
            {
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_MBSTAGS_FROMJSON + e.Message, ExceptionType.FAILTOPARSERESPONSE);
            }
        }

        public async Task<MBSTags> GetTagUID(string tagId)
        {
            if (CheckBasicVerification() == false)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_BASIC_VERIF, ExceptionType.BASICVERIF_FAILED);
            if (tagId == null || tagId.Length == 0)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + "Invalid Parameters", ExceptionType.INVALIDPARAMETERS);
            Debug.WriteLine(this.client.BaseAddress + "/tags/UniqueId/" + tagId);
            string result = await GetRequest("/tags/UniqueId/" + tagId);
            JObject resJson = JObject.Parse(result);

            if (resJson["error"] != null)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + resJson["error"].ToString(), ExceptionType.UNKNOWN);
            try
            {
                MBSTags tag = JsonConvert.DeserializeObject<MBSTags>(resJson["result"].ToString());

                if (tag == null)
                    return null;
                var res = await GetMMCParking(tag.id);
                if (res != null)
                    tag.parking = res;
                return tag;
            }
            catch (Exception e)
            {
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_MBSTAGS_FROMJSON + e.Message, ExceptionType.FAILTOPARSERESPONSE);
            }
        }

        public async Task<ulong> CreateTag(APP_Type type, ulong id, string tagUID, string name)
        {
            if (CheckBasicVerification() == false)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_BASIC_VERIF, ExceptionType.BASICVERIF_FAILED);
            if (tagUID == null || tagUID.Length == 0 || name == null || name.Length == 0)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + "Invalid Parameters", ExceptionType.INVALIDPARAMETERS);

            JObject postData = new JObject();
            postData["idApp"] = (int)type;
            postData["idUser"] = id;
            postData["TagUniqueId"] = tagUID;
            postData["Name"] = name;
            string result = await PostRequest(postData, "/tags/create");
            JObject resJson = JObject.Parse(result);
            if (resJson["error"] != null)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + resJson["error"].ToString(), ExceptionType.UNKNOWN);
            return Convert.ToUInt64(resJson["result"].ToString());
        }

        //Not working
        public async Task<bool> DeleteTag(ulong tagId)
        {
            if (CheckBasicVerification() == false)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_BASIC_VERIF, ExceptionType.BASICVERIF_FAILED);
           
            HttpResponseMessage response = await Instance.client.DeleteAsync(Instance.client.BaseAddress + "/tags/" + 20);
            string result = await response.Content.ReadAsStringAsync();
            Debug.WriteLine("DeleteTagResult: " + result);
            JObject resJson = JObject.Parse(result);
            if (resJson["error"] != null)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + (string)resJson["error"], ExceptionType.UNKNOWN);
            return false;
        }
        #endregion

        #region PushNotif
        
        //Push registration id to server GCM push
        public async Task<bool> CreatePush(string token)
        {
            if (CheckBasicVerification() == false)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_BASIC_VERIF, ExceptionType.BASICVERIF_FAILED);

            JObject postData = new JObject();
            postData["Registration"] = token;
            string result = await PostRequest(postData, "/push/create/" + this.currentUserId);
            Debug.WriteLine("ResultCreatePush: " + result);
            JObject resJson = JObject.Parse(result);
            if (resJson["error"] == null)
                return true;
            else
                return false;
        }

		public async Task<bool> SendPush(List<ulong> idUsers, Dictionary<string, string> messages)
        {
            if (CheckBasicVerification() == false)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR_BASIC_VERIF, ExceptionType.BASICVERIF_FAILED);
            JObject postData = new JObject();
			string ids = JsonConvert.SerializeObject(idUsers.ToArray());
			ids = ids.Replace ("\"", "");
			postData ["IdUsers"] = ids;
			postData["MessageObject"] = JsonConvert.SerializeObject(messages);
			string postContent = postData.ToString ();
			postContent = postContent.Replace ("\"[", "[");
			postContent = postContent.Replace ("]\"", "]");
			postContent = postContent.Replace ("\\", "");
			postContent = postContent.Replace ("\n", "");
			postContent = postContent.Replace ("\"{", "{");
			postContent = postContent.Replace ("}\"", "}");


			HttpContent content = new StringContent(postContent);
			HttpResponseMessage response = await Instance.client.PostAsync(Instance.client.BaseAddress + "/push/sendtousers/", content);
			string result = await response.Content.ReadAsStringAsync();
			JObject resJson = JObject.Parse(result);
			Debug.WriteLine ("Result sending Push: " + result);
            if (resJson["error"] != null)
                throw new MBSExceptionApi(Constants.ErrorAPIMessages.ERROR + (string)resJson["error"], ExceptionType.UNKNOWN);
            return true;
        }
        #endregion

    }
}
