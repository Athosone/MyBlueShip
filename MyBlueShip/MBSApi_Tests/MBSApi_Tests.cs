﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Cryptography;
using System.Text;
using MyBlueShip;
using MyBlueShip.API.DataModel;
using MyBlueShip.Common;
using System.Collections.Generic;

namespace MBSApi_Tests
{
    [TestClass]
    public class MBSApi_Tests
    {

        public static ulong tagIdToDelete;

        public static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA1.Create();  // SHA1.Create()
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        [TestMethod]
        public void getApps()
        {
            try
            {
                MBSApi.Instance.setInfosApp(APP_Type.MOBMYCAR, Platform_APP.iOS);
                var result = MBSApi.Instance.getApps();
                Console.WriteLine(result.Result);
                Assert.IsNotNull(result.Result);
                Assert.AreNotEqual<int>(0, result.Result.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine("Oops" + e);
                Assert.Fail(e.Message);
            }
        }

        //SUCCESS
        [TestMethod]
        public void LoginUser()
        {
            try
            {
                MBSApi.Instance.setInfosApp(APP_Type.MOBMYCAR, Platform_APP.iOS);
                string userpseudo = "Athosone";
                string password = GetHashString("myblueship");
                Console.WriteLine(password);
                var result = MBSApi.Instance.login(userpseudo, password);
                Console.WriteLine("Resul login: " + result.Result);
                Assert.IsNotNull(result.Result);
                Assert.AreEqual<string>("Athosone", result.Result.Pseudo);
                Assert.AreEqual<string>("Ayrton", result.Result.FirstName);
                Assert.AreEqual<string>("Athosone", result.Result.Pseudo);
                Assert.AreEqual<ulong>(29, result.Result.id);
            }
            catch (Exception e)
            {
                Console.WriteLine("Oops" + e);
                Assert.Fail(e.Message);
            }
        }

        //FAIL
        [TestMethod]
        public void LoginUserFail()
        {
            try
            {
                MBSApi.Instance.setInfosApp(APP_Type.MOBMYCAR, Platform_APP.iOS);
                string userpseudo = "Athosone";
                string password = GetHashString("myblueship111");
                Console.WriteLine(password);
                var result = MBSApi.Instance.login(userpseudo, password);
                Console.WriteLine("Resul login: " + result.Result);
                Assert.Fail("Login should not success");
            }
            catch (MBSExceptionApi e)
            {
                Console.WriteLine("Oops" + e);
                Assert.AreEqual<ExceptionType>(ExceptionType.INVALIDCREDENTIALS, e.type);
            }
        }

        [TestMethod]
        public void getAllTagsForCurrentUser()
        {
            try
            {
                MBSApi.Instance.setInfosApp(APP_Type.MOBMYCAR, Platform_APP.iOS);
                //userid param
                var result = MBSApi.Instance.GetAllTags(29);
                Console.WriteLine("Result getalltags: " + result.Result + "Count: " + result.Result.Count);
                Assert.IsNotNull(result.Result);
                MBSTags tag = result.Result[0];
                Assert.AreEqual<ulong>(29, tag.id_user);
                foreach (MBSTags t in result.Result)
                {
                    if (t.parking != null)
                    {
                        Assert.AreEqual<ulong>(t.id, t.parking.id_tag);
                        Console.WriteLine(t.id + " InParking: " + t.parking.InParking);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Oops" + e);
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void getTagWithId()
        {
            try
            {
                MBSApi.Instance.setInfosApp(APP_Type.MOBMYCAR, Platform_APP.iOS);
                var result = MBSApi.Instance.GetTag(20);
                Console.WriteLine("Result gettag: " + result.Result);
                Assert.IsNotNull(result.Result);
                MBSTags tag = result.Result;
                Assert.AreEqual<ulong>(29, tag.id_user);
            }
            catch (Exception e)
            {
                Console.WriteLine("Oops" + e);
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void getTagWithUniqueId()
        {
            try
            {
                MBSApi.Instance.setInfosApp(APP_Type.MOBMYCAR, Platform_APP.iOS);
                var result = MBSApi.Instance.GetTagUID("EvilTagUID");
                Console.WriteLine("Result gettagUID: " + result.Result);
                Assert.IsNotNull(result.Result);
                MBSTags tag = result.Result;
                Assert.AreEqual<ulong>(29, tag.id_user);
            }
            catch (Exception e)
            {
                Console.WriteLine("Oops" + e);
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void getMBSParking()
        {
            try
            {
                MBSApi.Instance.setInfosApp(APP_Type.MOBMYCAR, Platform_APP.iOS);
                var result = MBSApi.Instance.GetMMCParking(14);
                Console.WriteLine("Result getmbsparking: " + result.Result);
                Assert.IsNotNull(result.Result);
                MBSParking parking = result.Result;
                Assert.AreEqual<ulong>(1, parking.id);
            }
            catch (Exception e)
            {
                Console.WriteLine("Oops" + e);
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void GetUsersByRadius()
        {
            try
            {
                MBSApi.Instance.setInfosApp(APP_Type.MOBMYCAR, Platform_APP.iOS);
                MBSApi.Instance.currentUserId= 23;
                var result = MBSApi.Instance.GetCommnityUsers(45.8138920, 4.7594670, 5000);

                Console.WriteLine("Result GetUsersByRadius: " + result.Result.ToString());

                Assert.IsNotNull(result.Result);
                Assert.AreEqual<int>(2, result.Result.Count);
            }
            catch (Exception e)
            {
                Console.WriteLine("Oops" + e);
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void SendPush()
        {
            try
            {
                MBSApi.Instance.setInfosApp(APP_Type.MOBMYCAR, Platform_APP.iOS);
                Dictionary<string, string> dic = new Dictionary<string, string>(2);
                dic.Add("GOD", "Coucou");
                dic.Add("toto", "Coucou");
				List<ulong> ids = new List<ulong>(2);
				ids.Add(29);
				ids.Add(31);
                var res = MBSApi.Instance.SendPush(ids, dic);
            }
            catch (Exception e)
            {
                Console.WriteLine("Oops" + e);
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void GetUser()
        {
            try
            {
                MBSApi.Instance.setInfosApp(APP_Type.MOBMYCAR, Platform_APP.iOS);
                var result = MBSApi.Instance.GetUser(29);
                Console.WriteLine("Result getuser: " + result.Result);
                Assert.IsNotNull(result.Result);
                MBSUser user = result.Result;
                Assert.AreEqual<string>("Athosone", user.Pseudo);
            }
            catch (Exception e)
            {
                Console.WriteLine("Oops" + e);
                Assert.Fail(e.Message);
            }
        }

        /*[TestMethod]
        public void deleteTag()
        {
            try
            {
                MBSApi.Instance.setInfosApp(APP_Type.MOBMYCAR, Platform_APP.iOS);
                var result = MBSApi.Instance.DeleteTag(tagIdToDelete);
                Console.WriteLine("Result deletetag: " + result.Result);
                Assert.IsNotNull(result.Result);
            }
            catch (Exception e)
            {
                Console.WriteLine("Oops" + e);
                Assert.Fail(e.Message);
            }
        }*/
       
        [TestMethod]
        public void createTag()
        {
            try
            {
                MBSApi.Instance.setInfosApp(APP_Type.MOBMYCAR, Platform_APP.Android);
                var result = MBSApi.Instance.CreateTag(APP_Type.MOBMYCAR, 32, "06760C32E52C80" , "Peugeot 205CC");
                var result2 = MBSApi.Instance.CreateTag(APP_Type.MOBMYCAR, 33, "07760C32E52C80", "Audi TT");
                var result3 = MBSApi.Instance.CreateTag(APP_Type.MOBMYCAR, 34, "08760C32E52C80", "LandRover");

                Console.WriteLine("Result create: " + result.Result);
                Assert.AreNotEqual<ulong>(0, result.Result);
                tagIdToDelete = result.Result;
            }
            catch (Exception e)
            {
                Console.WriteLine("Oops" + e);
                Assert.Fail(e.Message);
            }
        }
    }
}
