﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;

namespace MyBlueShip.Droid
{
	using Android.Support.V4.App;
	using JavaString = Java.Lang.String;
			
	public class FragmentAdapterPresentation : FragmentPagerAdapter
	{

		public static JavaString[] CONTENT = new[]
		{
			new JavaString("1"),
			new JavaString("2")
		};

		private int mCount;

		public FragmentAdapterPresentation(FragmentManager fm)
			: base(fm)
		{
			mCount = CONTENT.Length;
		}

		public override int Count { get { return mCount; } }

		public override Android.Support.V4.App.Fragment GetItem(int position)
		{
			return new FragmentPresentationMobMyCar(CONTENT [position % CONTENT.Length].ToString (), position);
		}

		public override Java.Lang.ICharSequence GetPageTitleFormatted(int position)
		{
			return CONTENT [position % CONTENT.Length];
		}

		public void SetCount(int count)
		{
			if (count > 0 && count <= 10)
			{
				mCount = count;
				NotifyDataSetChanged();
			}
		}
	}
}

