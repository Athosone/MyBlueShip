﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using MyBlueShip.API.DataModel;
using Android.Content.PM;

namespace MyBlueShip.Droid
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar", Label = "ActivityMainPage", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class ActivityMainPage : ActivityIfMyUserIsLogged
	{
		ImageView	imageViewMyMob;
		ImageView	imageViewMyMobCommunity;
		ImageView	imageViewMyMobParking;
		ImageView	imageViewMyHorodateur;

		TextView	myMob;
		TextView	myMobCommunity;
		TextView	myMobParking;
		TextView	myHorodateur;

		int			myUserHasTag = 0;


		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.ActivityMainPage);

			//set the view
			imageViewMyMob = FindViewById<ImageView> (Resource.Id.myMobImageView);
			imageViewMyMobCommunity = FindViewById<ImageView> (Resource.Id.myMobCommunityImageView);
			imageViewMyMobParking = FindViewById<ImageView> (Resource.Id.myMobParkingImageView);
			imageViewMyHorodateur = FindViewById<ImageView> (Resource.Id.myHorodateurImageView);
			myMob = FindViewById <TextView> (Resource.Id.myMobString);
			myMobCommunity = FindViewById <TextView> (Resource.Id.myMobCommunityString);
			myMobParking = FindViewById <TextView> (Resource.Id.myMobParkingString);
			myHorodateur = FindViewById<TextView> (Resource.Id.myHorodateurString);

			//set the constant
			myMob.Text = Constants.StringApplication.LECTURE_QRCODE_OR_MOB;
			myMobCommunity.Text = Constants.StringApplication.MY_MOB_COMMUNITY;
			myMobParking.Text = Constants.StringApplication.MY_MOB_PARKING;
			myHorodateur.Text = Constants.StringApplication.MY_HORODATEUR;

			//set the typeface
			Typeface tf = Typeface.CreateFromAsset (Application.Context.Assets, Constants.TypefaceInfos.MY_BLUE_SHIP_TYPEFACE);
			myMob.Typeface = tf;
			myMobCommunity.Typeface = tf;
			myMobParking.Typeface = tf;
			myHorodateur.Typeface = tf;

			GetInfoTag ();

			//go to Activity ActivityMyMobs
			imageViewMyMob.Click += (object sender, EventArgs e) => {
				StartActivity(typeof(ActivityMyMobs));
			};

			//go to ActivityMyMobParking
			imageViewMyMobParking.Click += (object sender, EventArgs e) => {
				if (myUserHasTag == 1)
					StartActivity(typeof(ActivityMyEmplacement));
					
			};

			//go to ActivityMyMobCommunity
			imageViewMyMobCommunity.Click += (object sender, EventArgs e) => {
				StartActivity(typeof(ActivityMyCommunity));
			};

			//go to ActivityMyHorodateur
			imageViewMyHorodateur.Click += (object sender, EventArgs e) => {
				if (myUserHasTag == 1)
				{
					StartActivity(typeof(ActivityHorodateur));
				}
			};
		}

		public void MyUserNoHaveTag(){

			imageViewMyHorodateur.SetImageResource (Resource.Drawable.splah);
			imageViewMyMobParking.SetImageResource (Resource.Drawable.splah);

		}

		public override void OnBackPressed(){
			
			AlertDialog.Builder alert = new AlertDialog.Builder (this);

			alert.SetTitle (Constants.StringApplication.LOGOUT_TITLE);

			alert.SetPositiveButton (Constants.StringApplication.LOGOUT_CANCEL, (senderAlert, args) => {
				
			} );

			alert.SetNegativeButton (Constants.StringApplication.LOGOUT_OK, (senderAlert, args) => {
				MMCHelpers.ClearAllSharedPrefs();
				StartActivity(typeof(MainActivity));
				this.Finish();
			} );
			//run the alert in UI thread to display in the screen
			RunOnUiThread (() => {
				alert.Show();
			} );
		}

		public async void GetInfoTag(){

			//get all tag
			List<MBSTags>  tagForUser = await MBSApi.Instance.GetAllTags (MMCHelpers.GetCurrentUserIdPrefs());
			if (tagForUser == null) {
				MyUserNoHaveTag ();
			} else {
				myUserHasTag = 1;
			}
		}
	}
}

