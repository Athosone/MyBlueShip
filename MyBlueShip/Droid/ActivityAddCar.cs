﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using System.Threading.Tasks;
using MyBlueShip.Library.Android.NFC;
using MyBlueShip.Library.Android.Design;
using Android.Content.PM;

namespace MyBlueShip.Droid
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar", Label = "ActivityAddCar", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class ActivityAddCar : ActivityIfMyUserIsLogged
	{
		TextView	addMyModeleTitle;
		TextView	addMyCar;

		EditText	myPseudoEditText;
		EditText	myImmatriculationEditText;
		EditText	myCoordonneesEditText;
		EditText	myColorEditText;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.ActivityAddCar);

			//set the view
			addMyModeleTitle = FindViewById<TextView> (Resource.Id.chooseModeleCar);
			addMyCar = FindViewById<TextView> (Resource.Id.addACar);
			myPseudoEditText = FindViewById<EditText> (Resource.Id.myPseudoEditText);
			myImmatriculationEditText = FindViewById<EditText> (Resource.Id.myImmatriculationEditText);
			myCoordonneesEditText = FindViewById<EditText> (Resource.Id.myCoordonneesSIVEditText);
			myColorEditText = FindViewById<EditText> (Resource.Id.myColorEditText);

			//set the constant
			addMyModeleTitle.Text = Constants.StringApplication.CHOOSE_MY_CAR;
			addMyCar.Text = Constants.StringApplication.ADD_MY_CAR_BUTTON;
			myPseudoEditText.Hint = Constants.StringApplication.MY_PSEUDO;
			myImmatriculationEditText.Hint = Constants.StringApplication.MY_IMMATRICULATION;
			myCoordonneesEditText.Hint = Constants.StringApplication.MY_COORDONNEES;
			myColorEditText.Hint = Constants.StringApplication.MY_COLOR;

			//set the typeface
			Typeface tf = Typeface.CreateFromAsset (Application.Context.Assets, Constants.TypefaceInfos.MY_BLUE_SHIP_TYPEFACE);
			addMyModeleTitle.Typeface = tf;
			myPseudoEditText.Typeface = tf;
			myImmatriculationEditText.Typeface = tf;
			myCoordonneesEditText.Typeface = tf;
			myColorEditText.Typeface = tf;

			//Save a new car
			addMyCar.Click += (object sender, EventArgs e) => {

				string modeleMyCar = Intent.GetStringExtra ("Modele") ?? "Data not available";

				Console.WriteLine(modeleMyCar);

				if (modeleMyCar.Equals("1") || modeleMyCar.Equals("2") || modeleMyCar.Equals("3") || modeleMyCar.Equals("4"))
				{
				}
				else
					modeleMyCar = "2";

				if (myPseudoEditText.Text.Length == 0)
                    ToastCustom.MakeText(this, Constants.StringApplication.ERROR_LOGIN_EMPTY, ToastLength.Short).Show();
				else if (myImmatriculationEditText.Text.Length == 0)
					ToastCustom.MakeText (this, Constants.StringApplication.ERROR_IMMATRICULATION_EMPTY, ToastLength.Short).Show();
				else if (myCoordonneesEditText.Text.Length == 0)
					ToastCustom.MakeText (this, Constants.StringApplication.ERROR_COORDONNEES_SIV_EMPTY, ToastLength.Short).Show();
				else if (myColorEditText.Text.Length == 0)
					ToastCustom.MakeText (this, Constants.StringApplication.ERROR_COLOR_EMPTY, ToastLength.Short).Show();

				else{

					Task<int> sizeTask = AddACarAsynTask(myPseudoEditText.Text);

					//ToastCustom.MakeText (this, Constants.StringApplication.ADD_CAR_SUCCESS, ToastLength.Short).Show();
					//StartActivity (typeof(ActivityMainPage));
				}
			};

		}

		public override void OnBackPressed(){
			StartActivity (typeof(AddMyModeleCar));
		}

		//async Task for login
		public async Task<int> AddACarAsynTask(String name){

			ulong result = 0;

			Console.WriteLine (name);
			//Console.WriteLine ("id test" + MBSApi.Instance.currentUser.IDUser);

			try{
				result = await MBSApi.Instance.CreateTag (MyBlueShip.APP_Type.MOBMYCAR, MMCHelpers.GetCurrentUserIdPrefs(), "testUID", name);
			}
				catch (Exception e) {
					Console.WriteLine (e);
			}
			Console.WriteLine (result);

//			currentUser = await MBSApi.Instance.login (userLogin, userPassword);
			return 0;
		}
	}
}

