﻿using Android.App;
using Android.Gms.Gcm;
using Android.OS;

namespace MyBlueShip.Library.Android.GCM.HandleNotifications
{
    [Service(Exported = true), IntentFilter(new[] { "com.google.android.c2dm.intent.RECEIVE" })]
    public class MyGcmListenerService : GcmListenerService
    {

        public override void OnMessageReceived(string from, Bundle data)
        {
            string apptype = data.GetString("APP_Type");

            IHandleNotifications handle = null;
            switch (apptype)
            {
                case "1":
                    handle = new MMC_HandleNotifications(this);
                    break;
                default:
                    break;
            }
            if (handle != null)
                handle.HandleNotifications(data);
        }
    }
}