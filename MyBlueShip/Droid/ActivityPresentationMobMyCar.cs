﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Content.PM;

namespace MyBlueShip.Droid
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar", Label = "ActivityPresentationMobMyCar", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class ActivityPresentationMobMyCar : FragmentActivity
	{
		public FragmentAdapterPresentation 	fragmentAdapter;
		public ViewPager 					viewPager;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			SetContentView(Resource.Layout.ActivityPresentationMobMyCar);
			fragmentAdapter = new FragmentAdapterPresentation(SupportFragmentManager);
			viewPager = FindViewById<ViewPager>(Resource.Id.pagerPresentationMobMyCar);
			viewPager.Adapter = fragmentAdapter;
		}
	}
}

