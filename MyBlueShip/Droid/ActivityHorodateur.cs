﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using System.Threading.Tasks;
using MyBlueShip.API.DataModel;
using System.Threading;
using MyBlueShip.Library.Android.NFC;
using MyBlueShip.Library.Android.Design;
using Android.Content.PM;

namespace MyBlueShip.Droid
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar", Label = "ActivityHorodateur", ScreenOrientation = ScreenOrientation.Portrait)]
	//[IntentFilter(new[] { "android.nfc.action.TECH_DISCOVERED", "android.nfc.action.NDEF_DISCOVERED", "android.nfc.action.TAG_DISCOVERED" })]
	[MetaData("android.nfc.action.TECH_DISCOVERED", Resource = "@xml/filter_nfc")]
	public class ActivityHorodateur : MBSNFCLibraryRead
	{
		private MBSTags 	currentTag = null;

		private string 		latitude;
		private string 		longitude;

		ProgressDialog 		progress;

		LinearLayout		linearUp;
		LinearLayout		linearMiddle;
		LinearLayout		linearDown;

		TextView			infoHourCurrent;
		TextView			infoCar;
		TextView			infoHour;
		TextView			infoPrice;
		TextView			time0min;
		TextView			time2h;
		TextView			time4h;

		DateTime 			dateCurrent = DateTime.Today;
		DateTime 			timeCurrent = DateTime.Now;
		DateTime			timeEndPArking;

		SeekBar				seekBarHorodateur;

		Button				validate;
		Button				cancel;

		int					valueTime = 0;
		int					positionActivity = 0;
		int					possibleToTapMob = 0;
		int					modeAddPay = 0;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.ActivityHorodateur);


			//set the view
			infoHourCurrent = FindViewById<TextView> (Resource.Id.horodateurInfoHourCurrent);
			infoCar = FindViewById<TextView> (Resource.Id.horodateurInfoCar);
			infoHour = FindViewById<TextView> (Resource.Id.horodateurInfoHour);
			infoPrice = FindViewById<TextView> (Resource.Id.horodateurInfoPrice);
			time0min = FindViewById<TextView> (Resource.Id.horodateur0min);
			time2h = FindViewById<TextView> (Resource.Id.horodateur2h);
			time4h = FindViewById<TextView> (Resource.Id.horodateur4h);
			validate = FindViewById<Button> (Resource.Id.horodateurValidate);
			cancel = FindViewById<Button> (Resource.Id.horodateurCancel);
			seekBarHorodateur = FindViewById<SeekBar> (Resource.Id.seekBarHorodateur);
			linearUp = FindViewById<LinearLayout> (Resource.Id.linearUpHorodateur);
			linearMiddle = FindViewById<LinearLayout> (Resource.Id.linearDownHorodateur);
			linearDown = FindViewById<LinearLayout> (Resource.Id.LinearWithButtonCancelHorodateur);

			//set the constant
			infoHourCurrent.Text = dateCurrent.ToString ("dd MMMM yyyy") + "\n" + timeCurrent.ToString ("t");
			infoHour.Text = Constants.StringApplication.HORODATEUR_INFO_HOUR;
			infoPrice.Text = Constants.StringApplication.HORODATEUR_INFO_PRICE;
			time0min.Text = Constants.StringApplication.HORODATEUR_0_MIN;
			time2h.Text = Constants.StringApplication.HORODATEUR_2_H;
			time4h.Text = Constants.StringApplication.HORODATEUR_4_H;
			validate.Text = Constants.StringApplication.HORODATEUR_VALIDATE;
			cancel.Text = Constants.StringApplication.HORODATEUR_CANCEL;

			//set the visibility for linearLayout
			linearUp.Visibility = ViewStates.Visible;
			linearMiddle.Visibility = ViewStates.Gone;
			linearDown.Visibility = ViewStates.Gone;

			//set the typeface
			Typeface tf = Typeface.CreateFromAsset (Application.Context.Assets, Constants.TypefaceInfos.MY_BLUE_SHIP_TYPEFACE);
			infoHourCurrent.Typeface = tf;
			infoCar.Typeface = tf;
			infoHour.Typeface = tf;
			infoPrice.Typeface = tf;
			time0min.Typeface = tf;
			time2h.Typeface = tf;
			time4h.Typeface = tf;
			validate.Typeface = tf;
			cancel.Typeface = tf;

			//seekbar which change the cost and the time
			seekBarHorodateur.ProgressChanged += (object sender, SeekBar.ProgressChangedEventArgs e) => {
				if (e.FromUser)
				{

					Console.WriteLine("progress : " + e.Progress);

					if (e.Progress == 0)
					{
						float timeTopay = e.Progress;
						TimeSpan result = TimeSpan.FromMinutes(timeTopay);
						string fromTimeString = result.ToString("h'h'mm");

						DateTime combined = timeCurrent.Add(result);

						Double euroToPay = e.Progress / 60.00;
						Console.WriteLine(euroToPay);

						infoPrice.Text = fromTimeString + " = " + Math.Round(euroToPay, 2).ToString() + "€" +"\n" + combined.ToString("t");
						valueTime = e.Progress;
					}
					else{
						
					if (modeAddPay == 0)
                        {
					float timeTopay = e.Progress;
					TimeSpan result = TimeSpan.FromMinutes(timeTopay);
							Console.WriteLine("heure rajout : " + result);
					string fromTimeString = result.ToString("h'h'mm");

					DateTime combined = timeCurrent.Add(result);

					timeEndPArking = combined;

					Double euroToPay = e.Progress / 60.00;
					Console.WriteLine(euroToPay);

					infoPrice.Text = fromTimeString + " = " + Math.Round(euroToPay, 2).ToString() + "€" +"\n" + combined.ToString("t");
					valueTime = e.Progress;
					}
					else if (this.currentTag.parking != null){
						MBSTags tagCurrent = this.currentTag;

						float timeTopay = e.Progress;
						TimeSpan result = TimeSpan.FromMinutes(timeTopay);
						string fromTimeString = result.ToString("h'h'mm");

						DateTime dt = (DateTime)tagCurrent.parking.EndParking;

						DateTime combined = dt.Add(result);

						timeEndPArking = combined;

						Double euroToPay = e.Progress / 60.00;
						Console.WriteLine(euroToPay);

						infoPrice.Text = fromTimeString + " = " + Math.Round(euroToPay, 2).ToString() + "€" +"\n" + combined.ToString("t");
						valueTime = e.Progress;

						Console.WriteLine("temps finis :::::" + tagCurrent.parking.EndParking);
					}
					}
				}
			};

			//send the informations to park and pay for the user
			validate.Click += (object sender, EventArgs e) => {

				if (valueTime == 0)
					ToastCustom.MakeText (this, Constants.StringApplication.ERROR_HORODATEUR_EMPTY, ToastLength.Short).Show ();
				else if (this.currentTag != null && this.currentTag.parking != null)
				{
					AddPayementHorodateurAsyncTask();

				}
				else if (MMCHelpers.GetCurrentTagIdPrefs() > 0){
					PayementHorodateurAsyncTask();
				}
				else
				{
					LaunchProgressBar();
					ToastCustom.MakeText(this, "Vous devez scanner un MOB avant de pouvoir payer le parcmètre.", ToastLength.Short);
				}
			};

			//cancel the payement
			cancel.Click += (object sender, EventArgs e) => {
				CancelPayementHorodateurAsyncTask();
			};
			IsTagIsParkedAsyncTask ();

		}

		public void AddInfoFromTag(String nameTag){
			this.infoCar.Text = nameTag;
			if (progress != null && progress.IsShowing == true)
				progress.Dismiss ();

		}

		//scan the mob
		protected override void ResultReading(bool readEnd, MBSTags tag, string error, int status)
		{
			if (possibleToTapMob == 1) {
				//go to speak with owner tag
				if (status == Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_USER)
					ToastCustom.MakeText (this, Constants.StringApplication.LECTURE_MOB_TIERS, ToastLength.Short).Show ();
			//go to add a car
			else if (status == Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_NOUSER) {
					ToastCustom.MakeText (this, Constants.StringApplication.LECTURE_MOB_FOR_ME, ToastLength.Short).Show ();
					StartActivity (typeof(AddMyModeleCar));
				}
			//mob not from mob my car
			else if (status == Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_WRONGAPP)
					ToastCustom.MakeText (this, Constants.StringApplication.ERROR_MOB_NOT_FOR_MOB_MY_CAR, ToastLength.Short).Show ();
			//user already have the tag
			else if (status == Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_OWN) {

					if (MMCHelpers.GetCurrentTagIdPrefs() == tag.id || (this.currentTag != null && this.currentTag.id == tag.id)) {
						this.currentTag = tag;
						possibleToTapMob = 0;
						this.longitude = Convert.ToString (AppSingleton.Current.LocationService.lastLocation.Longitude);
						longitude = longitude.Replace (",", ".");
						this.latitude = Convert.ToString (AppSingleton.Current.LocationService.lastLocation.Latitude);
						latitude = latitude.Replace (",", ".");
						AddInfoFromTag (tag.Name);
					} else
						ToastCustom.MakeText (this, Constants.StringApplication.ERROR_TAG_OWN_BUT_NOT_CURRENT, ToastLength.Short).Show ();

				}
			//mob not from my blue ship
			else if (status == Constants.TAG_Owner_Statuts.BELONGS_NOT_TO_MBS)
					ToastCustom.MakeText (this, Constants.StringApplication.ERROR_MOB_NOT_FROM_MY_BLUE_SHIP, ToastLength.Short).Show ();
				else
					ToastCustom.MakeText (this, Constants.StringApplication.ERROR_MOB_NOT_FROM_MY_BLUE_SHIP, ToastLength.Short).Show ();
			}
		}

		public void AddPayementHorodateurAsyncTask(){

			MBSTags tagCurrent = this.currentTag;

			string currentlongitude = Convert.ToString(this.currentTag.parking.Longitude);
			currentlongitude = currentlongitude.Replace (",", ".");
			string currentlatitude = Convert.ToString(this.currentTag.parking.Latitude);
			currentlatitude = currentlatitude.Replace (",", ".");

			Console.WriteLine ("longtiude add : " + tagCurrent.parking.Longitude);

			List<KeyValuePair<string, string>> infoToSend = new List<KeyValuePair<string, string>>()
			{
				new KeyValuePair<string, string>("Longitude", currentlongitude),
				new KeyValuePair<string, string>("Latitude",  currentlatitude),
				new KeyValuePair<string, string>("EndParking", timeEndPArking.ToString("o")),
				new KeyValuePair<string, string>("InParking", "true"),
				new KeyValuePair<string, string>("Warning", "0"),
				new KeyValuePair<string, string>("SIV", "Siv"),
				new KeyValuePair<string, string>("id_icone", "icon"),
			};
			SendInformationForParkAsyncTask (infoToSend, tagCurrent.id, 0);
		}

		public void CancelPayementHorodateurAsyncTask(){

			MBSTags tagCurrent = this.currentTag;

			string currentlongitude = Convert.ToString(this.currentTag.parking.Longitude);
			currentlongitude = currentlongitude.Replace (",", ".");
			string currentlatitude = Convert.ToString(this.currentTag.parking.Latitude);
			currentlatitude = currentlatitude.Replace (",", ".");

			Console.WriteLine ("xaxa  " + tagCurrent.parking.Latitude);

			List<KeyValuePair<string, string>> infoToSend = new List<KeyValuePair<string, string>>()
			{
				new KeyValuePair<string, string>("Longitude", currentlongitude),
				new KeyValuePair<string, string>("Latitude",  currentlatitude),
				new KeyValuePair<string, string>("EndParking", timeEndPArking.ToString("o")),
				new KeyValuePair<string, string>("InParking", "false"),
				new KeyValuePair<string, string>("Warning", "0"),
				new KeyValuePair<string, string>("SIV", "Siv"),
				new KeyValuePair<string, string>("id_icone", "icon"),
			};
			SendInformationForParkAsyncTask (infoToSend, tagCurrent.id, 1);
		}

		public void PayementHorodateurAsyncTask(){

				List<KeyValuePair<string, string>> infoToSend = new List<KeyValuePair<string, string>>()
				{
					new KeyValuePair<string, string>("Longitude", longitude),
					new KeyValuePair<string, string>("Latitude",  latitude),
					new KeyValuePair<string, string>("EndParking", timeEndPArking.ToString("o")),
					new KeyValuePair<string, string>("InParking", "true"),
					new KeyValuePair<string, string>("Warning", "0"),
					new KeyValuePair<string, string>("SIV", "Siv"),
					new KeyValuePair<string, string>("id_icone", "icon"),
				};
			SendInformationForParkAsyncTask (infoToSend, MMCHelpers.GetCurrentTagIdPrefs(), 0);
		}

		//send information of the user for finish the payement
		public async void SendInformationForParkAsyncTask(List<KeyValuePair<string, string>> infoToSend, ulong tagId, int position){

			bool isParkOk = await MBSApi.Instance.UpdateParking (infoToSend, tagId);

			if (isParkOk)
			{
				MMCHelpers.SetCurrentTagIdPrefs (this.currentTag.id);
				if (position == 0) {
					ToastCustom.MakeText (this, Constants.StringApplication.HORODATEUR_NEW_PARK, ToastLength.Short).Show ();
					StartActivity (typeof(ActivityMainPage));
					this.Finish ();
				} else {
					ToastCustom.MakeText (this, Constants.StringApplication.HORODATEUR_CANCEL_PAYEMENT, ToastLength.Short).Show ();
					StartActivity (typeof(ActivityMainPage));
					this.Finish ();
				}
			}
			else
				ToastCustom.MakeText (this, Constants.StringApplication.ERROR_HORODATEUR_WRONG, ToastLength.Short).Show ();
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			if (this.currentTag != null)
				AddInfoFromTag (currentTag.Name);			
		}

		public override void OnBackPressed(){

			if (positionActivity == 0) {
				StartActivity (typeof(ActivityMainPage));
				this.Finish ();

			} else {
				progress.Dismiss ();
				StartActivity (typeof(ActivityMainPage));
				this.Finish ();
			}
		}

		public void LaunchProgressBar(){
			if (progress != null && progress.IsShowing == true)
				progress.Dismiss ();
			progress = null;
			progress = DesignComponent.CreateProgressDialog (this, Constants.StringApplication.HORODATEUR_SCAN_MOB, Constants.StringApplication.HORODATEUR_LOADING);
			progress.SetButton ((int)DialogButtonType.Negative, "Annuler", ((sender, EventArgs) => {
				this.Finish();
			}));
			progress.SetCancelable (false);
			progress.Show ();
		}

		public async void IsTagIsParkedAsyncTask(){

			//get all tag
			ulong currentTagId = MMCHelpers.GetCurrentTagIdPrefs();

			if (currentTagId == 0) {
				possibleToTapMob = 1;
				LaunchProgressBar ();
			} else {
				this.currentTag = await MBSApi.Instance.GetTag (currentTagId);
                if (this.currentTag == null)
                    return;
				AddInfoFromTag (currentTag.Name);
				if ((this.currentTag.parking != null && this.currentTag.parking.InParking == false) || this.currentTag.parking == null) {
					//Scan Mob
					possibleToTapMob = 1;
					LaunchProgressBar ();

				} else  {
					//the user has alread parked his car
					modeAddPay = 1;
					this.infoCar.Text = this.currentTag.Name;
					this.validate.Text = Constants.StringApplication.HORODATEUR_ADD;
					linearUp.Visibility = ViewStates.Gone;
					linearMiddle.Visibility = ViewStates.Visible;
					linearDown.Visibility = ViewStates.Visible;

				}
			}
		}

	}
}

