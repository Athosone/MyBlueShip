﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace MyBlueShip.Droid
{
	public class FragmentMyMobs : Fragment
	{

		TextView	applicationTitle;
		TextView	myMobs;
		TextView	addAMob;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate(Resource.Layout.ActivityMyMobs, container, false);

			//set the view
			applicationTitle = view.FindViewById <TextView> (Resource.Id.applicationTitle);
			myMobs = view.FindViewById<TextView> (Resource.Id.myMobs);
			addAMob = view.FindViewById<TextView> (Resource.Id.addAMob);

			//set the constant
			applicationTitle.Text = Constants.StringApplication.APPLICATION_TITLE;
			myMobs.Text = Constants.StringApplication.MY_MOBS;
			addAMob.Text = Constants.StringApplication.ADD_A_MOB;

			//set the typeface
			Typeface tf = Typeface.CreateFromAsset (Application.Context.Assets, Constants.TypefaceInfos.MY_BLUE_SHIP_TYPEFACE);
			applicationTitle.Typeface = tf;
			myMobs.Typeface = tf;
			addAMob.Typeface = tf;

			//go to Activity ActivityQrCodeOrMob
			addAMob.Click += (object sender, EventArgs e) => {
				this.Activity.StartActivity(typeof(ActivityLectureMob));
			};

			return view;
		}
	}
}

