﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using MyBlueShip.CrossLibrary;
using MyBlueShip.Library.Android.QRCode;
using Android.Graphics;
using System.Threading.Tasks;
using MyBlueShip.Library.Android.GCM;
using Android.Gms.Common;
using Android.Util;
using Android.Gms.Gcm.Iid;
using Android.Gms.Gcm;
using Java.IO;
using MyBlueShip.Library.Android;
using Android.Locations;
using Android.Nfc;
using MyBlueShip.Library.Android.Design;
using Android.Content.PM;

namespace MyBlueShip.Droid
{
	//[Activity (Label = "MyBlueShip.Droid", MainLauncher = true, Icon = "@drawable/icon")]
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar",Label = "MyBlueShip.Droid", ScreenOrientation = ScreenOrientation.Portrait)]//, IntentFilter(new[] { "android.nfc.action.TECH_DISCOVERED", "android.nfc.action.NDEF_DISCOVERED" })]	
	//[MetaData("android.nfc.action.TECH_DISCOVERED", Resource = "@xml/filter_nfc")]
	public class MainActivity : Activity
	{
		public const string TAG = "MainActivity";

		TextView	authentification;
		Button		buttonLogin;

		EditText	editTextLogin;
		EditText	editTextPassword;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
           
            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.Main);

			//set the view
			authentification = FindViewById<TextView> (Resource.Id.loginString);
			buttonLogin = FindViewById<Button> (Resource.Id.loginButton);
			editTextLogin = FindViewById<EditText> (Resource.Id.loginEditText);
			editTextPassword = FindViewById<EditText> (Resource.Id.passwordEditText);

			//set the constant
			authentification.Text = Constants.StringApplication.LOGIN_TITLE;
			editTextLogin.Hint = Constants.StringApplication.LOGIN_USER;
			editTextPassword.Hint = Constants.StringApplication.PASSWORD_USER;
			buttonLogin.Text = Constants.StringApplication.AUTHENTIFICATION;

			//set the typeface
			Typeface tf = Typeface.CreateFromAsset (Application.Context.Assets, Constants.TypefaceInfos.MY_BLUE_SHIP_TYPEFACE);
			authentification.Typeface = tf;
			editTextLogin.Typeface = tf;
			editTextPassword.Typeface = tf;
			buttonLogin.Typeface = tf;

			//editTextLogin.Text = "athosone";
			//editTextPassword.Text = "myblueship";

			editTextLogin.Text = "chaucola";
			editTextPassword.Text = "lenalee69";

			//button for login the user
			buttonLogin.Click += (object sender, EventArgs e) => {

				if (editTextLogin.Text.Length == 0)
					ToastCustom.MakeText (this, Constants.StringApplication.ERROR_LOGIN_EMPTY, ToastLength.Short).Show();
				else if (editTextPassword.Text.Length == 0)
					ToastCustom.MakeText (this, Constants.StringApplication.ERROR_PASSWORD_EMPTY, ToastLength.Short).Show();
				else
				{
					String passwordSHA1;

					passwordSHA1 = StringFunction.GetHashString(editTextPassword.Text);
                    System.Console.WriteLine(passwordSHA1);
					Task<int> sizeTask = LoginAsynTask(editTextLogin.Text, passwordSHA1);

					//// await! control returns to the caller
					//int length = await sizeTask;

				}
//				StartActivity(typeof(ActivityMainPage));
			};
		}

 
        protected override void OnResume ()
		{
			base.OnResume ();
			ulong userid = MMCHelpers.GetCurrentUserIdPrefs ();
			if (userid > 0) {
				MBSApi.Instance.currentUserId = userid;
				SuccessLogin ();
			}

		}

		public override void OnBackPressed(){

			AlertDialog.Builder alert = new AlertDialog.Builder (this);

			alert.SetTitle (Constants.StringApplication.CLOSE_APPLICATION_TITLE);

			alert.SetPositiveButton (Constants.StringApplication.CLOSE_APPLICATION_CANCEL, (senderAlert, args) => {
				//change value write your own set of instructions
				//you can also create an event for the same in xamarin
				//instead of writing things here
			} );

			alert.SetNegativeButton (Constants.StringApplication.CLOSE_APPLICATION_OK, (senderAlert, args) => {
				Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
			} );
			//run the alert in UI thread to display in the screen
			RunOnUiThread (() => {
				alert.Show();
			} );
		}
      

		//async Task for login
		public async Task<int> LoginAsynTask(String userLogin, String userPassword)
		{
			try {
				MBSUser currentUser = await MBSApi.Instance.login (userLogin, userPassword);
			

			//user is correct
			if (currentUser != null) {
				if (checkPlayServices () == true) {
					try 
					{
						MMCHelpers.SetCurrentUserIdPrefs(currentUser.id);
						//saved user login in shared preferences
                        this.StartService(new Intent(this, typeof(RegistrationIntentService)));
						SuccessLogin();
                    } catch (Exception ex)
					{
						Log.Debug(TAG, "Failed to complete token refresh", ex);
                        System.Console.WriteLine (ex.Message + " " + ex.HelpLink);
					}
				}
			}
			else
				ToastCustom.MakeText (this, Constants.StringApplication.ERROR_LOGIN_WRONG, ToastLength.Short).Show();
			}
			catch (Exception e) {
				System.Console.WriteLine (e);
			}
			return 0;
		}

		public void SuccessLogin()
		{
			StartActivity (typeof(ActivityMainPage));
		}

        public bool checkPlayServices() {
			int resultCode = GooglePlayServicesUtil.IsGooglePlayServicesAvailable(this);
			if (resultCode != ConnectionResult.Success) {
				if (GooglePlayServicesUtil.IsUserRecoverableError(resultCode)) {
					GooglePlayServicesUtil.GetErrorDialog(resultCode, this,
						9000).Show();
				} else {
					Log.Debug(TAG, "This device is not supported - Google Play Services.");
				}
				return false;
			}
			return true;
		}
	}
}