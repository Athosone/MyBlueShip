﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using MyBlueShip.Library.Android.GCM.HandleNotifications;
using MyBlueShip.API.DataModel;
using MyBlueShip.Library.Android.Design;
using Android.Content.PM;

namespace MyBlueShip.Droid
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar", Label = "ActivityDialogToAsk" ,ScreenOrientation = ScreenOrientation.Portrait)]			
	public class ActivityDialogToAsk : ActivityIfMyUserIsLogged
	{
		TextView	infoHourCurrent;
		TextView	infoMobUser;
		TextView	infoCommunication;
		TextView	infoImminentGo;
		TextView	infoParkProblem;
		TextView	infoCarProblem;

		DateTime 	dateCurrent = DateTime.Today;
		DateTime 	timeCurrent = DateTime.Now;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.ActivityDialogToAsk);

			//set the view
			infoHourCurrent = FindViewById<TextView> (Resource.Id.dialogToAskInfoHourCurrent);
			infoMobUser = FindViewById<TextView> (Resource.Id.dialogAskInfoMob);
			infoCommunication = FindViewById<TextView> (Resource.Id.dialogAskInfoCommunication);
			infoImminentGo = FindViewById<TextView> (Resource.Id.dialogAskImminentGo);
			infoParkProblem = FindViewById<TextView> (Resource.Id.dialogParkProblem);
			infoCarProblem = FindViewById<TextView> (Resource.Id.dialogAskCarProblem);

			//set the constant
			infoHourCurrent.Text = dateCurrent.ToString ("dd MMMM yyyy") + "\n" + timeCurrent.ToString ("t");
			infoMobUser.Text =  Intent.GetStringExtra ("name_tag") ?? "";
			infoCommunication.Text = Constants.StringApplication.DIALOG_ASK_COMMUNICATION;
			infoImminentGo.Text = Constants.StringApplication.DIALOG_ASK_IMMINENT_GO;
			infoParkProblem.Text = Constants.StringApplication.DIALOG_ASK_PARK_PROBLEM;
			infoCarProblem.Text = Constants.StringApplication.DIALOG_ASK_CAR_PROBLEM;

			//set the typeface
			Typeface tf = Typeface.CreateFromAsset (Application.Context.Assets, Constants.TypefaceInfos.MY_BLUE_SHIP_TYPEFACE);
			infoHourCurrent.Typeface = tf;
			infoMobUser.Typeface = tf;
			infoCommunication.Typeface = tf;
			infoImminentGo.Typeface = tf;
			infoParkProblem.Typeface = tf;
			infoCarProblem.Typeface = tf;

			//send the informations to the user owner
			infoCarProblem.Click += (object sender, EventArgs e) => {

				SendMessage(infoCarProblem.Text, TYPE_DIALOG.CAR_ISSUE);
				ToastCustom.MakeText (this, Constants.StringApplication.DIALOG_ASK_SEND_MESSAGE, ToastLength.Short).Show ();
				StartActivity (typeof(ActivityMainPage));
			};

			//send the informations to the user owner
			infoParkProblem.Click += (object sender, EventArgs e) => {

				SendMessage(infoParkProblem.Text, TYPE_DIALOG.ANNOYING_PARK);
				ToastCustom.MakeText (this, Constants.StringApplication.DIALOG_ASK_SEND_MESSAGE, ToastLength.Short).Show ();
				StartActivity (typeof(ActivityMainPage));
			};

			//send the informations to the user owner
			infoImminentGo.Click += (object sender, EventArgs e) => {
				SendMessage(infoCarProblem.Text, TYPE_DIALOG.LEAVING_SOON);
				ToastCustom.MakeText (this, Constants.StringApplication.DIALOG_ASK_SEND_MESSAGE, ToastLength.Short).Show ();
				StartActivity (typeof(ActivityMainPage));
			};

		}

		public async void SendMessage(string mess, TYPE_DIALOG typeDial)
		{
			ulong tagId = Convert.ToUInt64(Intent.GetFloatExtra ("id_tag", 0.0f));

			MBSTags tag = await MBSApi.Instance.GetTag (tagId);
			Dictionary<string, string> dic = new Dictionary<string, string>(1);

			ulong uid = MMCHelpers.GetCurrentUserIdPrefs ();
			dic.Add("APP_Type", "1");
			dic.Add("action", "DialogRequest");
			dic.Add("FromIdUser", Convert.ToString(uid));
			dic.Add("Message", mess);
			dic.Add ("Type", Convert.ToString((int)typeDial));

			List<ulong> ids = new List<ulong>(1);

			ids.Add (tag.id_user);
			await MBSApi.Instance.SendPush(ids, dic);
		}

		public override void OnBackPressed(){
			StartActivity (typeof(ActivityMainPage));
			this.Finish ();
		}
	}
}

