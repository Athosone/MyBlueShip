﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using MyBlueShip.Library.Android.NFC;
using MyBlueShip.API.DataModel;
using System.Threading.Tasks;
using Android.Nfc;
using MyBlueShip.Library.Android.Design;
using Android.Content.PM;

namespace MyBlueShip.Droid
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar", Label = "MOBmyCAR", ScreenOrientation = ScreenOrientation.Portrait), 
		IntentFilter(new[] { "android.nfc.action.TECH_DISCOVERED", "android.nfc.action.NDEF_DISCOVERED", "android.nfc.action.TAG_DISCOVERED" }, DataHost= "www.mobmycar.com", DataScheme= "http", 
			Categories = new[] { Intent.CategoryDefault, Intent.CategoryBrowsable }),
		IntentFilter(new[] { "android.nfc.action.TECH_DISCOVERED", "android.nfc.action.NDEF_DISCOVERED", "android.nfc.action.TAG_DISCOVERED" })]	
	[MetaData("android.nfc.action.TECH_DISCOVERED", Resource = "@xml/filter_nfc")]
	public class ActivityLectureMob : MBSNFCLibraryRead
    {
		TextView	lectureMyMobTitle;
		TextView	lectureMyMobContent;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.ActivityLectureMob);

			//set the view
			lectureMyMobTitle = FindViewById<TextView> (Resource.Id.lectureMobTitle);
			lectureMyMobContent = FindViewById<TextView> (Resource.Id.lectureMobContent);

			//set the constant
			lectureMyMobTitle.Text = Constants.StringApplication.LECTURE_MY_MOB_TITLE;
			lectureMyMobContent.Text = Constants.StringApplication.LECTURE_MY_MOB_CONTENT;

			//set the typeface
			Typeface tf = Typeface.CreateFromAsset (Application.Context.Assets, Constants.TypefaceInfos.MY_BLUE_SHIP_TYPEFACE);
			lectureMyMobTitle.Typeface = tf;
			lectureMyMobContent.Typeface = tf;

			//var videoView = FindViewById<VideoView> (Resource.Id.videoLectureMyMob);

			//var uri = Android.Net.Uri.Parse ("http://ia600507.us.archive.org/25/items/Cartoontheater1930sAnd1950s1/PigsInAPolka1943.mp4");

			//videoView.SetVideoURI (uri);
			//videoView.Visibility = ViewStates.Visible;
			//videoView.Start ();

			lectureMyMobContent.Click += (object sender, EventArgs e) => {
				StartActivity(typeof(AddMyModeleCar));
			};

		}

		protected override void OnResume()
		{
			base.OnResume ();
			ulong uid = MMCHelpers.GetCurrentUserIdPrefs ();
			if (uid == 0) {
				ToastCustom.MakeText (this, "Vous devez être connecté pour accéder à se service", ToastLength.Short).Show ();
				StartActivity (typeof(MainActivity));
				this.Finish ();
			}
			AppSingleton.StartLocationService ();
			MBSApi.Instance.currentUserId = uid;
			if (NfcAdapter.ActionTechDiscovered.Equals(Intent.Action) || NfcAdapter.ActionNdefDiscovered.Equals(Intent.Action) || NfcAdapter.ActionTagDiscovered.Equals(Intent.Action)) {
				base.NfcRead (Intent);
			}

		}

		public void FetchCurrentUserForNfcRead()
		{
		}

		public override void OnBackPressed(){
			StartActivity (typeof(ActivityMyMobs));
			this.Finish ();
		}

		protected override void ResultReading(bool readEnd, MBSTags tag, string error, int status)
        {
			Console.WriteLine ("test status" + status);
			//Console.WriteLine ("id app zzzzzz   :" + tag.id_app);

			//go to speak with owner tag
			if (status == Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_USER) {
				ToastCustom.MakeText (this, Constants.StringApplication.LECTURE_MOB_TIERS, ToastLength.Short).Show ();

				var intent = new Intent(this, typeof(ActivityDialogToAsk));
				intent.PutExtra("id_tag", tag.id);
				intent.PutExtra ("name_tag", tag.Name);
				StartActivity(intent);
//				StartActivity (typeof(ActivityDialogToAsk));
			}
			//go to add a car
			else if (status == Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_NOUSER) {
				ToastCustom.MakeText (this, Constants.StringApplication.LECTURE_MOB_FOR_ME, ToastLength.Short).Show ();
				StartActivity (typeof(AddMyModeleCar));
			} 
			//mob not from mob my car
			else if (status == Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_WRONGAPP) {
				ToastCustom.MakeText (this, Constants.StringApplication.ERROR_MOB_NOT_FOR_MOB_MY_CAR, ToastLength.Short).Show ();
				StartActivity (typeof(ActivityMainPage));
			}
			//user already have the tag
			else if (status == Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_OWN) {


				//saved the location in shared preferences
				ISharedPreferences prefs = Application.Context.GetSharedPreferences ("USERS_PREFS", FileCreationMode.Private);

				ISharedPreferencesEditor editor = prefs.Edit();
				editor.PutFloat ("user_latitude", (float)AppSingleton.Current.LocationService.lastLocation.Latitude);
				editor.PutFloat("user_longitude", (float)AppSingleton.Current.LocationService.lastLocation.Longitude);
				editor.Apply();
				editor.Commit();

				MMCHelpers.SetCurrentTagIdPrefs (tag.id);

				ToastCustom.MakeText (this, tag.Name + Constants.StringApplication.LECTURE_MOB_ALREADY_FOR_ME, ToastLength.Short).Show ();
				StartActivity (typeof(ActivityMainPage));
			}
			//mob not from my blue ship
			else if (status == Constants.TAG_Owner_Statuts.BELONGS_NOT_TO_MBS) {
				ToastCustom.MakeText (this, Constants.StringApplication.ERROR_MOB_NOT_FROM_MY_BLUE_SHIP, ToastLength.Short).Show ();
				StartActivity (typeof(ActivityMainPage));
			} else {
				ToastCustom.MakeText (this, Constants.StringApplication.ERROR_MOB_NOT_FROM_MY_BLUE_SHIP, ToastLength.Short).Show ();
				StartActivity (typeof(ActivityMainPage));
				this.Finish ();
			}
        }




		//async Task for login
		public async Task<int> LectureMyMobAsynTask(ulong uid){

			try{
				MBSTags tag = await  MBSApi.Instance.GetTag (uid);
			}
			catch (Exception e) {
				Console.WriteLine (e);
			}

			return 0;
		}
    }
}

