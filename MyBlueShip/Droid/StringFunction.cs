﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Security.Cryptography;

namespace MyBlueShip.Droid
{
	[Activity (Label = "StringFunction")]			
	public class StringFunction : Activity
	{
		//create SHA1 from string
		public static byte[] GetHash(string inputString)
		{
			// SHA1.Create()
			HashAlgorithm algorithm = SHA1.Create();  
			return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
		}

		//create string from SHA1
		public static string GetHashString(string inputString)
		{
			StringBuilder sb = new StringBuilder();
			foreach (byte b in GetHash(inputString))
				sb.Append(b.ToString("X2"));

			return sb.ToString();
		}

	}
}

