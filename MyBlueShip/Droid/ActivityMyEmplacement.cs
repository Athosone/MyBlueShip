﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Locations;
using MyBlueShip.API.DataModel;
using MyBlueShip.Library.Android.MAP;
using MyBlueShip.Common;
using MyBlueShip.Library.Android.NFC;
using Android.Graphics;
using MyBlueShip.Library.Android.Design;
using Android.Content.PM;

namespace MyBlueShip.Droid
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar", Label = "ActivityMyEmplacement", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class ActivityMyEmplacement : MBSNFCLibraryRead, IOnMapReadyCallback
	{
		private List<KeyValuePair<string, MBSParking>> 	markerParking;
		private ProgressDialog 							progress;
		private MapFragment 							_mapFragment;
		private GoogleMap 								_map;
		private MBSTags 								currentTag = null;
		private int 									i = 0;
		private int										possibleToTapMob = 0;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.ActivityMyEmplacement);

			InitMapFragment();

			IsTagIsParkedAsyncTask ();
			//GetTagForUser();

		}

		public override void OnBackPressed(){
			StartActivity (typeof(ActivityMainPage));
			this.Finish ();
		}

		protected override void OnResume()
		{
			base.OnResume();

		}

		//scan the mob
		protected override void ResultReading(bool readEnd, MBSTags tag, string error, int status)
		{
			if (possibleToTapMob == 1) {
				if (status == Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_USER)
					ToastCustom.MakeText (this, Constants.StringApplication.LECTURE_MOB_TIERS, ToastLength.Short).Show ();
				else if (status == Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_NOUSER)
					ToastCustom.MakeText (this, Constants.StringApplication.LECTURE_MOB_FOR_ME, ToastLength.Short).Show ();
				else if (status == Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_WRONGAPP)
					ToastCustom.MakeText (this, Constants.StringApplication.ERROR_MOB_NOT_FOR_MOB_MY_CAR, ToastLength.Short).Show ();
				else if (status == Constants.TAG_Owner_Statuts.BELONGS_TO_MBS_OWN) {
					if (MMCHelpers.GetCurrentTagIdPrefs() == tag.id || (this.currentTag != null && this.currentTag.id == tag.id)) {
						possibleToTapMob = 0;
						progress.Dismiss ();
						this.currentTag = tag;
						UpdateCamera (this.currentTag);
						MBSApi.Instance.UpdateParking (new List<KeyValuePair<string, string>> () {
							new KeyValuePair<string, string>("Longitude", MMCHelpers.GetCurrentLongString() ?? "0"),
							new KeyValuePair<string, string>("Latitude",  MMCHelpers.GetCurrentLatString() ?? "0"),
							new KeyValuePair<string, string>("EndParking", "2100-01-01 00:00:00.0000000"),
							new KeyValuePair<string, string>("InParking", "true"),
							new KeyValuePair<string, string>("Warning", "0"),
							new KeyValuePair<string, string>("SIV", "Siv"),
							new KeyValuePair<string, string>("id_icone", "icon"),
						}, this.currentTag.id);
					} else {
						DesignComponent.CreateAlertDialog (this, "", "Définir ce mob comme mob actuel?", "Définir comme actuel", "Non, tap un autre mob",
							new EventHandler<DialogClickEventArgs> ((sender, args) => {
								this.currentTag = tag;
								MMCHelpers.SetCurrentTagIdPrefs (tag.id);
								possibleToTapMob = 0;
								UpdateCamera (currentTag);
								MBSApi.Instance.UpdateParking (new List<KeyValuePair<string, string>> () {
									new KeyValuePair<string, string>("Longitude", MMCHelpers.GetCurrentLongString() ?? "0"),
									new KeyValuePair<string, string>("Latitude",  MMCHelpers.GetCurrentLatString() ?? "0"),
									new KeyValuePair<string, string>("EndParking", "2100-01-01 00:00:00.0000000"),
									new KeyValuePair<string, string>("InParking", "true"),
									new KeyValuePair<string, string>("Warning", "0"),
									new KeyValuePair<string, string>("SIV", "Siv"),
									new KeyValuePair<string, string>("id_icone", "icon"),
								}, this.currentTag.id);
								progress.Dismiss ();
								((AlertDialog)sender).Cancel ();
							}), new EventHandler<DialogClickEventArgs> ((sender, args) => {
							LaunchProgressBar ();
							((AlertDialog)sender).Cancel ();
						})).Show ();
						//ToastCustom.MakeText (this, Constants.StringApplication.ERROR_TAG_OWN_BUT_NOT_CURRENT, ToastLength.Short).Show ();
					}
				}
				//mob not from my blue ship
				else if (status == Constants.TAG_Owner_Statuts.BELONGS_NOT_TO_MBS)
					ToastCustom.MakeText (this, Constants.StringApplication.ERROR_MOB_NOT_FROM_MY_BLUE_SHIP, ToastLength.Short).Show ();
				else
					ToastCustom.MakeText (this, Constants.StringApplication.ERROR_MOB_NOT_FROM_MY_BLUE_SHIP, ToastLength.Short).Show ();
			}
		}

		private void InitMapFragment()
		{
			_mapFragment = FragmentManager.FindFragmentByTag("map") as MapFragment;
			if (_mapFragment == null)
			{
				GoogleMapOptions mapOptions = new GoogleMapOptions()
					.InvokeMapType(GoogleMap.MapTypeSatellite)
					.InvokeZoomControlsEnabled(false)
					.InvokeCompassEnabled(true);

				FragmentTransaction fragTx = FragmentManager.BeginTransaction();
				_mapFragment = MapFragment.NewInstance(mapOptions);
				fragTx.Add(Resource.Id.map, _mapFragment, "map");
				fragTx.Commit();
			}

			_mapFragment.GetMapAsync(this);
		}

		public void OnMapReady(GoogleMap googleMap)
		{
			this._map = googleMap;
			if (_map != null)
			{
				this._map.MapType = GoogleMap.MapTypeNormal;
				this._map.UiSettings.ZoomControlsEnabled = true;
				this._map.UiSettings.CompassEnabled = true;
				this._map.UiSettings.MyLocationButtonEnabled = true;
				_map.MyLocationEnabled = true;
			}
		}

		private CameraUpdate MoveCameraToPosition(float latitude, float longitude)
		{

			CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
			//builder.Target(new LatLng(location.Latitude, location.Longitude));
			builder.Target(new LatLng(latitude, longitude));
			builder.Zoom(Constants.LocationInfos.ZOOM_CAMERA);
			CameraPosition cameraPosition = builder.Build();
			CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
			return cameraUpdate;
		}

		public void CreateMarkers(MBSTags currentTag)
		{
			Console.WriteLine ("llcecececee   : " + currentTag.parking.Latitude);
			Console.WriteLine ("id tag    " + currentTag.id);
			if (this.markerParking == null)
				this.markerParking = new List<KeyValuePair<string, MBSParking>>();
			this.markerParking.Clear();

			MarkerOptions markerOpt = new MarkerOptions();
			markerOpt.SetPosition(new LatLng(currentTag.parking.Latitude, currentTag.parking.Longitude));
			//markerOpt.SetIcon(BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueGreen));
			markerOpt.SetIcon(BitmapDescriptorFactory.FromResource(MMCHelpers.GetResourceIdIconAvatar(currentTag.parking.id_icone ?? 0)));
			markerOpt.Flat(true);
			Marker mark = this._map.AddMarker(markerOpt);
/*			this.markerParking.Add(new KeyValuePair<string, MBSParking>(mark.Id, AppSingleton.Current.currentTag.parking));*/
		}

		/*public void HandleLocationChanged(object sender, LocationChangedEventArgs e)
		{
			System.Console.WriteLine ("Latitudeeeee  " + e.Location.Latitude);
		}*/

		public void LaunchProgressBar(){
		
			if (progress != null && progress.IsShowing == true)
				progress.Dismiss ();
			progress = null;
			progress = DesignComponent.CreateProgressDialog (this, Constants.StringApplication.MY_EMPLACEMENT, Constants.StringApplication.HORODATEUR_LOADING);
			progress.SetButton ((int)DialogButtonType.Negative, "Annuler", ((sender, EventArgs) => {
				this.Finish();
			}));
			progress.SetCancelable (false);
			progress.Show ();	
		}

		public void UpdateCamera(MBSTags currentTag){
			
			CreateMarkers (currentTag);
			CameraUpdate updatecam = this.MoveCameraToPosition((float)AppSingleton.Current.LocationService.lastLocation.Latitude, (float)AppSingleton.Current.LocationService.lastLocation.Longitude);
			if (i < 2)
			{
				this._map.MoveCamera(updatecam);
				i += 1;
			}

		}

		public async void IsTagIsParkedAsyncTask(){

			//get all tag
			ulong currentTagId = MMCHelpers.GetCurrentTagIdPrefs ();

			if (currentTagId == 0) {
				possibleToTapMob = 1;
				LaunchProgressBar ();

			} else {
				this.currentTag = await MBSApi.Instance.GetTag (currentTagId);

				if (this.currentTag.parking != null && this.currentTag.parking.InParking == false) {
					//Scan Mob
					possibleToTapMob = 1;
					LaunchProgressBar ();

				} else {
					CreateMarkers (this.currentTag);
					CameraUpdate updatecam = this.MoveCameraToPosition((float)AppSingleton.Current.LocationService.lastLocation.Latitude, (float)AppSingleton.Current.LocationService.lastLocation.Longitude);
					if (i < 2)
					{
						this._map.MoveCamera(updatecam);
						i += 1;
					}
				}
			}
		}

		/*//get the current tag of the user 
		public async void GetTagForUser(){

			//get all tag
			List<MBSTags>  tagForUser = await MBSApi.Instance.GetAllTags (MBSApi.Instance.currentUserId);


			if (tagForUser == null) {
				ToastCustom.MakeText (this, Constants.StringApplication.HORODATEUR_CAR_EMPTY, ToastLength.Short).Show ();
			} else {

				MBSTags tagCurrent = tagForUser.First();

				if (tagCurrent == null)
					ToastCustom.MakeText (this, Constants.StringApplication.ERROR_NOT_CURRENT_TAG, ToastLength.Short).Show ();
				else {
					AppSingleton.Current.currentTag = tagCurrent;
					CreateMarkers (tagCurrent);
					CameraUpdate updatecam = this.MoveCameraToPosition((float)AppSingleton.Current.LocationService.lastLocation.Latitude, (float)AppSingleton.Current.LocationService.lastLocation.Longitude);
					if (i < 2)
					{
						this._map.MoveCamera(updatecam);
						i += 1;
					}
				}
			}

		}*/
	}
}

