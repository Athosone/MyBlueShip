﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using MyBlueShip.API.DataModel;
using Android.Content.PM;
using MyBlueShip.Library.Android.Design;

namespace MyBlueShip.Droid
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar", Label = "MyBlueShip.Droid", ScreenOrientation = ScreenOrientation.Portrait)]
	public class ActivityMyMobs : ActivityIfMyUserIsLogged
	{
		List<MBSTags>	listMob = new List<MBSTags>();
		ListView		listView;

		TextView		myMobs;

		Button			addAMob;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.ActivityMyMobs);

			GetAllTagUserAsyncTask ();

			//set the view
			myMobs = FindViewById<TextView> (Resource.Id.myMobs);
			addAMob = FindViewById<Button> (Resource.Id.addAMob);
			listView = FindViewById<ListView>(Resource.Id.listViewMyMobs); 

			//set the constant
			myMobs.Text = Constants.StringApplication.MY_MOBS;
			addAMob.Text = Constants.StringApplication.ADD_A_MOB;

			//set the typeface
			Typeface tf = Typeface.CreateFromAsset (Application.Context.Assets, Constants.TypefaceInfos.MY_BLUE_SHIP_TYPEFACE);
			myMobs.Typeface = tf;
			addAMob.Typeface = tf;

			//go to Activity ActivityQrCodeOrMob
			addAMob.Click += (object sender, EventArgs e) => {
				//StartActivity(typeof(MyBlueShip.Library.Android.NFC.MBSNFCLibraryRead));
				StartActivity(typeof(ActivityLectureMob));
			};
		}

		public async void GetAllTagUserAsyncTask(){

			ulong id_user = MMCHelpers.GetCurrentUserIdPrefs ();

			this.listMob = await MBSApi.Instance.GetAllTags(id_user);
			if (this.listMob != null && this.listMob.Capacity > 0)
				SetMyListOfMob ();

		}

		public void SetMyListOfMob(){

			Console.WriteLine ("e test a l'ancienne");
			listView.Adapter = new MyMobsAdapter(this, this.listMob);
			listView.ItemClick += OnListItemClick;

		}


		void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			var listView = sender as ListView;
			var t = listMob[e.Position];
			MMCHelpers.SetCurrentTagIdPrefs (t.id);
			ToastCustom.MakeText (this, "Vous avez choisi:", ToastLength.Short, t.Name ?? "").Show();
			this.Finish ();
		}

		public override void OnBackPressed(){
			StartActivity (typeof(ActivityMainPage));
			this.Finish ();
		}

	}
}

