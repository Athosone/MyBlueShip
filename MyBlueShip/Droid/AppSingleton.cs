﻿using System;
using MyBlueShip.Library.Android;
using System.Threading.Tasks;
using Android.Content;
using MyBlueShip.API.DataModel;
using MyBlueShip.Droid.Notifications;
using MyBlueShip.Library.Android.Events;

namespace MyBlueShip.Droid
{
	public class AppSingleton
	{
		public MBSTags currentTag { get; set; }

		private static volatile AppSingleton _instance = null;
		private static object syncRoot = new Object ();
		public delegate void LocationServiceConnectedHandler (object sender, ServiceConnectedEventArgs e);
		public event LocationServiceConnectedHandler LocationServiceConnected;
		LocationServiceConnection locationServiceConnection;
		public MapDataProviderServices LocationService;

        #region Event vars
        public event EventHandler<ReceiveParkingResponseEvent> parkingResponseEvent = delegate { };
        public event EventHandler<StatusConnectivityEventArgs> statusConnectivityEvent = delegate { };

        #endregion
        private AppSingleton ()	{
			locationServiceConnection = new LocationServiceConnection (null);

			locationServiceConnection.ServiceConnected += (object sender, ServiceConnectedEventArgs e) => {
				LocationServiceBinder b = e.Binder as LocationServiceBinder;
				if (b != null)
					this.LocationService = b.Service;
				if (this.LocationServiceConnected != null)
					this.LocationServiceConnected (this, e);
			};
		}

		public static AppSingleton Current 
		{
			get {
					if (_instance == null) {
						lock (syncRoot) {
							if (_instance == null)
								_instance = new AppSingleton ();
						}
					}
					return _instance;
				}
		}

        #region Location Event

        public static void StartLocationService()
		{
			LocationServiceConnection locationServiceConnection = AppSingleton.Current.locationServiceConnection;
			new Task (() => {
				Android.App.Application.Context.StartService (new Intent (Android.App.Application.Context, typeof (MapDataProviderServices)));
				Intent locationServiceIntent = 	new Intent (Android.App.Application.Context, typeof (MapDataProviderServices));
				Android.App.Application.Context.BindService (locationServiceIntent, locationServiceConnection, Bind.AutoCreate);
			} ).Start ();
		}

		public static void StopLocationService ()
		{
			LocationServiceConnection locationServiceConnection = AppSingleton.Current.locationServiceConnection;

			if (locationServiceConnection != null)
				Android.App.Application.Context.UnbindService (locationServiceConnection);
			if (Current.LocationService != null)
				Current.LocationService.StopSelf ();
		}

        #endregion

        #region Parking 

        public void NewParkingResponse(Intent i)
		{
			this.parkingResponseEvent(this, new ReceiveParkingResponseEvent(i));
		}

        #endregion

        #region StatusServiceConnectivity Events

        public void NewGpsDisabledEvent(StatusConnectivityType type)
        {
            this.statusConnectivityEvent(this, new StatusConnectivityEventArgs(type));
        }

        #endregion

    }
}

