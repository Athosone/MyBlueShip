﻿using System;
using Android.Content;
using Android.App;
using Android.Graphics.Drawables;
using Android.Graphics;

namespace MyBlueShip.Droid
{
	public class MMCHelpers
	{
		public static ulong GetCurrentUserIdPrefs()
		{
			ISharedPreferences prefs = Application.Context.GetSharedPreferences ("USERS_PREFS", FileCreationMode.Private);

			ulong userid = (ulong)prefs.GetLong (Constants.StringApplication.CURRENT_USER_ID, 0L);
			return userid;
		}

		public static void SetCurrentUserIdPrefs(ulong id = 0)
		{
			ISharedPreferences prefs = Application.Context.GetSharedPreferences ("USERS_PREFS", FileCreationMode.Private);

			ISharedPreferencesEditor editor = prefs.Edit();
			editor.PutLong(Constants.StringApplication.CURRENT_USER_ID, Convert.ToInt64(id));
			editor.Apply();
			editor.Commit();
		}


		public static ulong GetCurrentTagIdPrefs()
		{
			ISharedPreferences prefs = Application.Context.GetSharedPreferences (Constants.StringApplication.TAGS_PREF, FileCreationMode.Private);

			ulong tagid = (ulong)prefs.GetLong ("CURRENT_TAG_ID", 0L);
			return tagid;
		}

		public static void SetCurrentTagIdPrefs(ulong id = 0)
		{
			ISharedPreferences prefs = Application.Context.GetSharedPreferences (Constants.StringApplication.TAGS_PREF, FileCreationMode.Private);

			ISharedPreferencesEditor editor = prefs.Edit();
			editor.PutLong("CURRENT_TAG_ID", Convert.ToInt64(id));
			editor.Apply();
			editor.Commit();
		}


		public static void ClearAllSharedPrefs()
		{
			ISharedPreferences prefsUser = Application.Context.GetSharedPreferences ("USERS_PREFS", FileCreationMode.Private);
			prefsUser.Edit ().Clear ().Commit ();
			ISharedPreferences prefsTags = Application.Context.GetSharedPreferences (Constants.StringApplication.TAGS_PREF, FileCreationMode.Private);
			prefsTags.Edit ().Clear ().Commit ();
		}

		#region Location

		public static string GetCurrentLatString()
		{
			string lRet = null;
			if (AppSingleton.Current.LocationService != null && AppSingleton.Current.LocationService.lastLocation != null) {
				string lat = Convert.ToString (AppSingleton.Current.LocationService.lastLocation.Latitude);
				lRet = lat.Replace (",", ".");
			}
			return lRet;
		}

		public static string GetCurrentLongString()
		{
			string lRet = null;
			if (AppSingleton.Current.LocationService != null && AppSingleton.Current.LocationService.lastLocation != null) {
				string longitude = Convert.ToString (AppSingleton.Current.LocationService.lastLocation.Longitude);
				lRet = longitude.Replace (",", ".");
			}
			return lRet;
		}

		#endregion
		public static int GetResourceIdIconAvatar(int Id)
		{
			switch (Id) {
			case 1:
				return Resource.Drawable.icon_size_1_normal;
			case 2:
				return Resource.Drawable.icon_size_2_normal;
			case 3:
				return Resource.Drawable.icon_size_3_normal;
			case 4:
				return Resource.Drawable.icon_size_4_normal;
			default:
				return Resource.Drawable.icon_size_1_normal;
			}

		}

		#region Image
	
		public static BitmapDrawable ScaleImage(Drawable image, Android.Content.Res.Resources res, float scaleFactor) {
			if ((image == null) || (image.GetType() != typeof(BitmapDrawable))) {
				return null;
			}

			Bitmap b = ((BitmapDrawable)image).Bitmap;

			int sizeX = (int)Math.Round(image.IntrinsicWidth * scaleFactor);
			int sizeY = (int)Math.Round(image.IntrinsicHeight * scaleFactor);

			Bitmap bitmapResized = Bitmap.CreateScaledBitmap(b, sizeX, sizeY, false);

			BitmapDrawable imageRet = new BitmapDrawable(res, bitmapResized);

			return imageRet;
		}

		#endregion
	}
}

