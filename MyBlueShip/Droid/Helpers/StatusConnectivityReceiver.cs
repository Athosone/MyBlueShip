using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyBlueShip.Library.Android.Events;
using Android.Locations;
using Android.Nfc;

namespace MyBlueShip.Droid.Helpers
{
    [BroadcastReceiver]
    [IntentFilter(new[] { "android.location.PROVIDERS_CHANGED", "android.nfc.action.ADAPTER_STATE_CHANGED" })]
    public class StatusConnectivityReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            if (intent.Action.Equals("android.location.PROVIDERS_CHANGED"))
            {
                LocationManager locationManager = (LocationManager)context.GetSystemService(Context.LocationService);
                if (locationManager.IsProviderEnabled(LocationManager.GpsProvider) == false)
                {
                    AppSingleton.Current.NewGpsDisabledEvent(StatusConnectivityType.NOGPS);
                }
            }
            else if (intent.Action.Equals("android.nfc.action.ADAPTER_STATE_CHANGED"))
            {
                NfcManager manager = (NfcManager)context.GetSystemService(Context.NfcService);

                if (manager.DefaultAdapter.IsEnabled == false)
                {
                    AppSingleton.Current.NewGpsDisabledEvent(StatusConnectivityType.NONFC);
                }
            }
        }
    }
}