﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Content.PM;

namespace MyBlueShip.Droid
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar", Label = "AddMyModeleCar", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class AddMyModeleCar : ActivityIfMyUserIsLogged
	{
		ImageView	modeleCar1;
		ImageView	modeleCar2;
		ImageView	modeleCar3;
		ImageView	modeleCar4;

		TextView	addMyModeleTitle;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.ActivityAddModeleCar);

			//set the view
			modeleCar1 = FindViewById<ImageView> (Resource.Id.modeleCarImageView1);
			modeleCar2 = FindViewById<ImageView> (Resource.Id.modeleCarImageView2);
			modeleCar3 = FindViewById<ImageView> (Resource.Id.modeleCarImageView3);
			modeleCar4 = FindViewById<ImageView> (Resource.Id.modeleCarImageView4);
			addMyModeleTitle = FindViewById<TextView> (Resource.Id.chooseModeleCar);

			//set the constant
			addMyModeleTitle.Text = Constants.StringApplication.CHOOSE_MY_MODELE_CAR;

			//set the typeface
			Typeface tf = Typeface.CreateFromAsset (Application.Context.Assets, Constants.TypefaceInfos.MY_BLUE_SHIP_TYPEFACE);
			addMyModeleTitle.Typeface = tf;

			//go to Activity ActivityAddCar
			modeleCar1.Click += (object sender, EventArgs e) => {
				var activityAddMyCar = new Intent (this, typeof(ActivityAddCar));
				activityAddMyCar.PutExtra ("Modele", "1");
				StartActivity (activityAddMyCar);
			};

			//go to Activity ActivityAddCar
			modeleCar2.Click += (object sender, EventArgs e) => {
				var activityAddMyCar = new Intent (this, typeof(ActivityAddCar));
				activityAddMyCar.PutExtra ("Modele", "2");
				StartActivity (activityAddMyCar);
			};

			//go to Activity ActivityAddCar
			modeleCar3.Click += (object sender, EventArgs e) => {
				var activityAddMyCar = new Intent (this, typeof(ActivityAddCar));
				activityAddMyCar.PutExtra ("Modele", "3");
				StartActivity (activityAddMyCar);
			};

			//go to Activity ActivityAddCar
			modeleCar4.Click += (object sender, EventArgs e) => {
				var activityAddMyCar = new Intent (this, typeof(ActivityAddCar));
				activityAddMyCar.PutExtra ("Modele", "4");
				StartActivity (activityAddMyCar);
			};
		}

		public override void OnBackPressed(){
			StartActivity (typeof(ActivityMyMobs));
		}

	}
}

