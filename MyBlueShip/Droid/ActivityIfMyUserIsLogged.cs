﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyBlueShip.Library.Android.Design;
using MyBlueShip.Library.Android;
using Android.Locations;
using Android.Util;
using Android.Nfc;
using MyBlueShip.Library.Android.Events;

namespace MyBlueShip.Droid
{
	[Activity (Label = "ActivityIfMyUserIsLogged")]			
	public class ActivityIfMyUserIsLogged : Activity
	{
        private AlertDialog currentDialog;
        private bool isPaused = false;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			ulong uid = MMCHelpers.GetCurrentUserIdPrefs ();
			if (uid == 0) {
				ToastCustom.MakeText (this, Constants.StringApplication.ERROR_LOGIN_EMPTY, ToastLength.Short).Show();
				StartActivity (typeof(MainActivity));
				this.Finish ();
			}
            //Register for LocationUpdates Need to stop service when app is done !
            AppSingleton.Current.LocationServiceConnected += (object sender, ServiceConnectedEventArgs e) => {
                Log.Debug("MapDataProviderServices", "ServiceConnected Event Raised");

                AppSingleton.Current.LocationService.StartLocationUpdates();

                AppSingleton.Current.LocationService.LocationChanged += HandleLocationChanged;
                AppSingleton.Current.LocationService.GPSDisabled += HandleStatusEventConnectivityEvent;
                AppSingleton.Current.statusConnectivityEvent += HandleStatusEventConnectivityEvent;
            };
            AppSingleton.StartLocationService();
        }

        protected override void OnResume()
		{
			base.OnResume ();
            isPaused = false;
			ulong uid = MMCHelpers.GetCurrentUserIdPrefs ();
			if (uid == 0) {
				ToastCustom.MakeText (this, Constants.StringApplication.ERROR_LOGIN_EMPTY, ToastLength.Short).Show();	

				StartActivity (typeof(MainActivity));
				this.Finish ();
			}
			AppSingleton.StartLocationService ();
            NfcManager manager = (NfcManager)GetSystemService(Context.NfcService);

            if (manager.DefaultAdapter.IsEnabled == false)
            {
                if (currentDialog != null && currentDialog.IsShowing == true)
                    currentDialog.Cancel();
                currentDialog = DesignComponent.CreateAlertDialog(this, (int)AlertDialogType.NONFC);
                currentDialog.Show();
            }
            
        }

		protected override void OnPause()
		{
			base.OnPause ();
            isPaused = true;
			AppSingleton.StopLocationService ();
		}

        private void HandleStatusEventConnectivityEvent(object sender, Library.Android.Events.StatusConnectivityEventArgs e)
        { 
            if (currentDialog != null && currentDialog.IsShowing == true)
                currentDialog.Cancel();
            if (isPaused == true)
                return;
            currentDialog = DesignComponent.CreateAlertDialog(this, (int)e.type);
            currentDialog.Show();
        }

        public void HandleLocationChanged(object sender, LocationChangedEventArgs e)
        {
            System.Console.WriteLine("Latitudeeeee  " + e.Location.Latitude);
        }
    }
}

