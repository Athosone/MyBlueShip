﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace MyBlueShip.Droid
{
	public class FragmentPresantation : Android.Support.V4.App.Fragment
	{

		private const string KEY_CONTENT = "TestFragment:Content";
		private string mContent = "???";

		private ImageView 	background;
		private ImageView	background2;
		private Button 		goToLoginButton;
		private int			position;

		public FragmentPresantation(int position) {
			this.position = position;
		}

		public FragmentPresantation(string content, int position)
		{
			var builder = new StringBuilder();
			for (var i = 0; i < 20; i++)
			{
				if (i != 19)
				{
					builder.Append(content).Append(" ");
				}
				else
				{
					builder.Append(content);
				}
			}
			mContent = builder.ToString();
			this.position = position;
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			if ((savedInstanceState != null) && savedInstanceState.ContainsKey(KEY_CONTENT))
			{
				mContent = savedInstanceState.GetString(KEY_CONTENT);
			}

			// Use this to return your custom view for this Fragment
			var view = inflater.Inflate(Resource.Layout.FragmentPresentation, container, false);

			//set the view
			this.background = view.FindViewById<ImageView> (Resource.Id.presentationImageView);
			this.background2 = view.FindViewById<ImageView> (Resource.Id.presentationWithButtonImageView);
			this.goToLoginButton = view.FindViewById<Button> (Resource.Id.presentationButton);

			//set the constant
			this.goToLoginButton.Text = Constants.StringApplication.LOGIN_PRESENTATION;

			//set the typeface
			Typeface tf = Typeface.CreateFromAsset (Application.Context.Assets, Constants.TypefaceInfos.MY_BLUE_SHIP_TYPEFACE);
			this.goToLoginButton.Typeface = tf;

			if (this.position == 0) {
				this.background2.Visibility = ViewStates.Gone;
				this.background.Visibility = ViewStates.Visible;
				this.goToLoginButton.Visibility = ViewStates.Gone;

				this.background.SetImageResource (Resource.Drawable.splah);
			}
			if (this.position == 1) {
				this.background2.Visibility = ViewStates.Gone;
				this.background.Visibility = ViewStates.Visible;
				this.goToLoginButton.Visibility = ViewStates.Gone;

				this.background.SetImageResource (Resource.Drawable.logo_icon_128x128);
			} 
			if (this.position == 2) {
				this.background2.Visibility = ViewStates.Gone;
				this.background.Visibility = ViewStates.Visible;
				this.goToLoginButton.Visibility = ViewStates.Gone;

				this.background.SetImageResource (Resource.Drawable.logo_icon_128x128);
			}
			if (this.position == 3) {
				this.background2.Visibility = ViewStates.Visible;
				this.background.Visibility = ViewStates.Gone;
				this.goToLoginButton.Visibility = ViewStates.Visible;

				this.background2.SetImageResource (Resource.Drawable.splah);
			}

			//go to Activity ActivityQrCodeOrMob
			goToLoginButton.Click += (object sender, EventArgs e) => {
				this.Activity.StartActivity(typeof(ActivityMainPage));
			};

			return view;

		}

		public override void OnSaveInstanceState(Bundle outState)
		{
			base.OnSaveInstanceState(outState);
			outState.PutString(KEY_CONTENT, mContent);
		}
	}
}

