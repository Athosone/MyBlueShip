﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyBlueShip.API.DataModel;

namespace MyBlueShip.Droid
{			
	public class MyMobsAdapter : BaseAdapter<MBSTags>
	{
		List<MBSTags> 	items;
		Activity 		context;

		public MyMobsAdapter(Activity context, List<MBSTags> items)
			: base()
		{
			this.context = context;
			this.items = items;
		}
		public override long GetItemId(int position)
		{
			return position;
		}
		public override MBSTags this[int position]
		{
			get { return items[position]; }
		}
		public override int Count
		{
			get { return items.Count; }
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var item = items[position];
			View view = convertView;
			if (view == null) // no view to re-use, create new
				view = context.LayoutInflater.Inflate(Resource.Layout.RowMyMob, null);
			view.FindViewById<TextView>(Resource.Id.Text1).Text = item.Name;
			if (item.Name == "Land Rover")
				view.FindViewById<ImageView>(Resource.Id.avatarMyMob).SetImageResource(Resource.Drawable.avatar_size_3_normal);
			else if (item.Name == "Bentley")
				view.FindViewById<ImageView>(Resource.Id.avatarMyMob).SetImageResource(Resource.Drawable.avatar_size_2_normal);
			//view.FindViewById<TextView>(Resource.Id.Text2).Text = item.SubHeading;
			//view.FindViewById<ImageView>(Resource.Id.Image).SetImageResource(item.ImageResourceId);
			return view;
		}
	}
}

