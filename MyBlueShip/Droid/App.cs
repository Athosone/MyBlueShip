﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyBlueShip.Library.Android;
using System.Threading.Tasks;

namespace MyBlueShip.Droid
{
	[Application]
	public class App : Application
	{
		
		public App (IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
		{
		}

		public override void OnCreate ()
		{
			base.OnCreate ();

			MBSApi.Instance.setInfosApp(APP_Type.MOBMYCAR, Platform_APP.Android);
		}
	}
}

