﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace MyBlueShip.Droid
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar", Label = "ActivityAddMyCar")]			
	public class ActivityAddMyCar : ActivityIfMyUserIsLogged
	{

		TextView	applicationTitle;
		TextView	addMyCarTitle;
		TextView	chooseMyCar;

		EditText	myImmatriculationEditText;
		EditText	myModeleEditText;
		EditText	myCoordonneesEditText;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.ActivityAddMyCar);

			//set the view
			applicationTitle = FindViewById <TextView> (Resource.Id.applicationTitle);
			addMyCarTitle = FindViewById<TextView> (Resource.Id.addMyCarTitle);
			chooseMyCar = FindViewById<TextView> (Resource.Id.chooseMyCar);
			myImmatriculationEditText = FindViewById<EditText> (Resource.Id.myImmatriculationEditText);
			myModeleEditText = FindViewById<EditText> (Resource.Id.myModeleEditText);
			myCoordonneesEditText = FindViewById<EditText> (Resource.Id.myCoordonneesEditText);

			//set the constant
			applicationTitle.Text = Constants.StringApplication.APPLICATION_TITLE;
			addMyCarTitle.Text = Constants.StringApplication.ADD_MY_CAR_TITLE;
			chooseMyCar.Text = Constants.StringApplication.CHOOSE_MY_CAR;
			myImmatriculationEditText.Hint = Constants.StringApplication.MY_IMMATRICULATION;
			//myModeleEditText.Hint = Constants.StringApplication.MY_MODELE;
			myCoordonneesEditText.Hint = Constants.StringApplication.MY_COORDONNEES;

			//set the typeface
			Typeface tf = Typeface.CreateFromAsset (Application.Context.Assets, Constants.TypefaceInfos.MY_BLUE_SHIP_TYPEFACE);
			applicationTitle.Typeface = tf;
			addMyCarTitle.Typeface = tf;
			chooseMyCar.Typeface = tf;
			myImmatriculationEditText.Typeface = tf;
			myModeleEditText.Typeface = tf;
			myCoordonneesEditText.Typeface = tf;
		}
	}
}

