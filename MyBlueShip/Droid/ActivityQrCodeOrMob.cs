﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace MyBlueShip.Droid
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar", Label = "ActivityQrCodeOrMob")]			
	public class ActivityQrCodeOrMob : Activity
	{

		ImageView	imageViewLectureMob;
		ImageView	imageViewLectureQrCode;

		TextView	applicationTitle;
		TextView	lectureMob;
		TextView	lectureQrCode;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.ActivityQrCodeOrMob);

			//set the view
			imageViewLectureMob = FindViewById<ImageView> (Resource.Id.lectureMobImageView);
			imageViewLectureQrCode = FindViewById<ImageView> (Resource.Id.lectureQrCodeImageView);
			applicationTitle = FindViewById<TextView> (Resource.Id.applicationTitle);
			lectureMob = FindViewById<TextView> (Resource.Id.lectureMobString);
			lectureQrCode = FindViewById<TextView> (Resource.Id.lectureQrCodeString);

			//set the constant
			applicationTitle.Text = Constants.StringApplication.APPLICATION_TITLE;
			lectureMob.Text = Constants.StringApplication.LECTURE_MOB;
			lectureQrCode.Text = Constants.StringApplication.LECTURE_QRCODE;

			//set the typeface
			Typeface tf = Typeface.CreateFromAsset (Application.Context.Assets, Constants.TypefaceInfos.MY_BLUE_SHIP_TYPEFACE);
			applicationTitle.Typeface = tf;
			lectureMob.Typeface = tf;
			lectureQrCode.Typeface = tf;

			//go to Activity ActivityLectureQrCode
			imageViewLectureQrCode.Click += (object sender, EventArgs e) => {
				StartActivity(typeof(ActivityLectureQrCode));
			};

			//go to Activity ActivityLectureMob
			imageViewLectureMob.Click += (object sender, EventArgs e) => {
				StartActivity(typeof(ActivityLectureMob));
			};

		}
	}
}

