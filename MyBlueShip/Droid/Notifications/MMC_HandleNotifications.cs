using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using System.Threading.Tasks;
using MyBlueShip.Droid.Notifications;
using Android.Support.V4.Content;
using MyBlueShip.Droid;
using Android.Graphics;

namespace MyBlueShip.Library.Android.GCM.HandleNotifications
{

	public enum TYPE_DIALOG 
	{
		ANNOYING_PARK = 1,
		CAR_ISSUE = 2,
		LEAVING_SOON = 3,
		THANKS = 4,
		NONE = 5
	}

	public enum NOTIF_ID
	{
		PARKING = 1,
		DIALOG = 10
	}

    public class MMC_HandleNotifications : IHandleNotifications
    {
        private Context context;
		private  Dictionary<string, Delegate> routingNotif;
       
		public MMC_HandleNotifications(Context ctx)
        {
            context = ctx;
			var dico = new Dictionary<string, Delegate> ();
			dico["ParkingRequest"] = new Func<object, Task<bool>>(ParkingRequest);
			dico["ParkingResponse"] = new Func<object, Task<bool>>(ParkingResponse);
			dico["DialogRequest"] = new Func<object, Task<bool>>(DialogRequest);

			this.routingNotif = dico;
		}

        public void HandleNotifications(Bundle data)
        {
			string action = data.GetString ("action");
			this.routingNotif [action].DynamicInvoke (data);
        }

		/*dic.Add("APP_Type", "1");
			dic.Add("action", "DialogRequest");
			dic.Add("FromIdUser", Convert.ToString(uid));
			dic.Add("Message", mess);
			dic.Add("Type", type);//1 , 2 , 3
			*/
		public async Task<bool> DialogRequest(object data)
		{
			Bundle infos = (Bundle)data;
			Bundle extras = new Bundle ();
			ulong idUser = Convert.ToUInt64 (infos.GetString ("FromIdUser"));
			string mess = infos.GetString ("Message") ?? "";
			string type = infos.GetString ("Type");


			extras.PutLong ("FromIdUser", (long)idUser);


			MBSUser user = await MBSApi.Instance.GetUser(idUser);
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder (context)
				.SetSmallIcon (Resource.Drawable.button_main_page_my_mob_normal)
				.SetLargeIcon (BitmapFactory.DecodeResource (context.Resources, Resource.Drawable.button_main_page_my_mob_normal))
				.SetContentTitle ("Information")
				.SetContentText (user.Pseudo + " " + mess);

			switch (Convert.ToInt16(type)) {
			case (int)TYPE_DIALOG.ANNOYING_PARK:
				mBuilder.AddAction (Resource.Drawable.notification_merci_white, "Merci", getPendingActionDialog (context, "ANNOYING_PARK", typeof(DialogReceiver), extras));
				mBuilder.SetPriority (NotificationCompat.PriorityMax);
				break;
			case (int)TYPE_DIALOG.CAR_ISSUE:
				mBuilder.AddAction(Resource.Drawable.notification_merci_white, "Merci", getPendingActionDialog(context, "CAR_ISSUE", typeof(DialogReceiver), extras));
				mBuilder.SetPriority (NotificationCompat.PriorityMax);
				break;
			case (int)TYPE_DIALOG.LEAVING_SOON:
				mBuilder.AddAction (Resource.Drawable.notification_refuser_white, "Non", getPendingActionDialog (context, "LEAVING_SOON_NONE", typeof(DialogReceiver), extras));
				mBuilder.AddAction (Resource.Drawable.notification_alarme_white, "-10 min", getPendingActionDialog (context, "LEAVING_SOON_YES", typeof(DialogReceiver), extras));
				mBuilder.SetContentText (user.Pseudo + " souhaite prendre votre place");
				mBuilder.SetPriority (NotificationCompat.PriorityMax);
				break;
			case (int)TYPE_DIALOG.THANKS:
				mBuilder.SetContentText (user.Pseudo + " vous remercie !");
				mBuilder.SetPriority (NotificationCompat.PriorityLow);
				break;
			default:
				break;
			}
				

			Notification notification = mBuilder.Build ();
			notification.Defaults |= NotificationDefaults.All;
			notification.Flags |= NotificationFlags.AutoCancel;

			NotificationManager mNotificationManager =
				(NotificationManager)context.GetSystemService(Context.NotificationService);
			mNotificationManager.Notify((int)NOTIF_ID.DIALOG, notification);
			return true;

		}

		/*	long uid = pref.GetLong ("user_id", Convert.ToInt64(0));
			dic.Add("APP_Type", "1");
			dic.Add("action", "ParkingResponse");
			dic.Add("Response", "KO");
			dic.Add("FromIdUser", Convert.ToString(uid));
			List<ulong> ids = new List<ulong>(1);
			ids.Add(Convert.ToUInt64(uid));*/
		public async Task<bool> ParkingResponse(object data)
		{
			Bundle infos = (Bundle)data;
			ulong idUser = Convert.ToUInt64 (infos.GetString ("FromIdUser"));
			string response = infos.GetString ("Response");

			MBSUser user = await MBSApi.Instance.GetUser(idUser);
			Intent intent = new Intent(context, typeof(ParkingResponseReceiver));
			intent.SetAction (Constants.PushNotfication.INTENT_FILTER_PARKING_RESPONSE);

			intent.PutExtra ("RESPONSE", response);
			intent.PutExtra ("USER_ID", idUser);

            context.SendBroadcast(intent);
            return true;// success;
		}


		/*long uid = pref.GetLong ("user_id", Convert.ToInt64(0));
		dic.Add("APP_Type", "1");
		dic.Add("action", "ParkingRequest");
		dic.Add("FromIdUser", Convert.ToString(uid));
		List<ulong> ids = new List<ulong>(1);
		ids.Add(tags.id_user);*/
    	public async Task<bool> ParkingRequest(object data)
		{
			Bundle infos = (Bundle)data;
			ulong idUser = Convert.ToUInt64 (infos.GetString ("FromIdUser"));

            MBSUser user = await MBSApi.Instance.GetUser(idUser);
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder (context)
				.SetSmallIcon (Resource.Drawable.button_main_page_my_mob_normal)
				.SetLargeIcon (BitmapFactory.DecodeResource (context.Resources, Resource.Drawable.button_main_page_my_mob_normal))
				//TEST
				.SetPriority (NotificationCompat.PriorityMax)
             .SetContentTitle ("Stationnement Communautaire")
             .SetContentText (user.Pseudo + " souhaite prendre votre place")
				.AddAction (Resource.Drawable.notification_accepter_white, "Accepter", getPendingActionParking (context, "APPROVED", idUser))
				.AddAction (Resource.Drawable.notification_refuser_white, "Rejeter", getPendingActionParking (context, "DENIED", idUser));
			
			Notification notification = mBuilder.Build ();
			notification.Defaults |= NotificationDefaults.All;
			notification.Flags |= NotificationFlags.AutoCancel;
			NotificationManager mNotificationManager =
                (NotificationManager)context.GetSystemService(Context.NotificationService);
			mNotificationManager.Notify((int)NOTIF_ID.PARKING, notification);
			
            return true;
		}

		public PendingIntent getPendingActionParking(Context context, string status, ulong toUserId = 0) {
			// Prepare intent which is triggered if the
			// notification is selected
			Intent intent = new Intent(context, typeof(ParkingRequestHandler));
			intent.SetAction (status);
			intent.PutExtra ("REQUEST", status);
			intent.PutExtra ("NOTIF_ID", (int)NOTIF_ID.PARKING);
			if (toUserId != 0)
				intent.PutExtra ("TOUSERID", toUserId);
			
			return PendingIntent.GetBroadcast(context, 0, intent, PendingIntentFlags.UpdateCurrent);
		}

		public PendingIntent getPendingActionDialog(Context context, string action, Type type, Bundle extras) {
			// Prepare intent which is triggered if the
			// notification is selected
			Intent intent = new Intent(context, type);
			intent.SetAction (action);
			extras.PutInt ("NOTIF_ID", (int)NOTIF_ID.DIALOG);
			intent.PutExtras (extras);
			return PendingIntent.GetBroadcast(context, 0, intent, PendingIntentFlags.OneShot);
		}

    }
}