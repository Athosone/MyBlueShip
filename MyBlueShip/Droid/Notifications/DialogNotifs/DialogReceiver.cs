﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyBlueShip.Library.Android.GCM.HandleNotifications;

namespace MyBlueShip.Droid
{
	[BroadcastReceiver]
	[IntentFilter(new []  {"ANNOYING_PARK", "CAR_ISSUE", "LEAVING_SOON_NONE", "LEAVING_SOON_YES", "THANKS"})]
	public class DialogReceiver : NotificationReceiver
	{
		private Intent mIntent;
		private Context ctx;
		private ulong toUserId;

		public override void OnReceive (Context context, Intent intent)
		{
			base.OnReceive (context, intent);
			this.mIntent = intent;
			this.ctx = context;
			this.toUserId = Convert.ToUInt64(intent.GetLongExtra("FromIdUser", 0L));

			if (toUserId == 0.0f)
				return;
			switch (intent.Action) 
			{
			case "ANNOYING_PARK":
				AnnoyingPark ();
				break;
			case "CAR_ISSUE":
				CarIssue ();
				break;
			case "LEAVING_SOON_NONE":
				LeavingSoonNone ();
				break;
			case "LEAVING_SOON_YES":
				LeavingSoonYes ();
				break;
			default:
				break;
			}
		}
	
		private async void sayThanks()
		{
			Dictionary<string, string> dic = new Dictionary<string, string> ();

			ulong uid = MMCHelpers.GetCurrentUserIdPrefs ();

			dic.Add("APP_Type", "1");
			dic.Add("action", "DialogRequest");
			dic.Add("FromIdUser", Convert.ToString(uid));
			dic.Add("Type", Convert.ToString((int)TYPE_DIALOG.THANKS));
			List<ulong> ids = new List<ulong> (1);
			ids.Add (this.toUserId);
			await MBSApi.Instance.SendPush (ids, dic);
		}

		private void AnnoyingPark()
		{
			sayThanks ();
		}
	
		private void CarIssue()
		{
			sayThanks ();
		}

		private async void LeavingSoonNone()
		{
			Dictionary<string, string> dic = new Dictionary<string, string> ();

			ulong uid = MMCHelpers.GetCurrentUserIdPrefs ();

			dic.Add("APP_Type", "1");
			dic.Add("action", "DialogRequest");
			dic.Add ("Message", "ne quitte pas sa place");
			dic.Add("FromIdUser", Convert.ToString(uid));
			dic.Add("Type", Convert.ToString((int)TYPE_DIALOG.NONE));
			List<ulong> ids = new List<ulong> (1);
			ids.Add (this.toUserId);
			await MBSApi.Instance.SendPush (ids, dic);
		}

		private async void LeavingSoonYes()
		{
			Dictionary<string, string> dic = new Dictionary<string, string> ();

			ulong uid = MMCHelpers.GetCurrentUserIdPrefs ();

			dic.Add("APP_Type", "1");
			dic.Add("action", "DialogRequest");
			dic.Add ("Message", "s'en va d'ici 10 minutes");
			dic.Add("FromIdUser", Convert.ToString(uid));
			dic.Add("Type", Convert.ToString((int)TYPE_DIALOG.NONE));
			List<ulong> ids = new List<ulong> (1);
			ids.Add (this.toUserId);
			await MBSApi.Instance.SendPush (ids, dic);
		}

	}
}

