﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyBlueShip.Library.Android.GCM.HandleNotifications;

namespace MyBlueShip.Droid
{
	[BroadcastReceiver]
	public class NotificationReceiver : BroadcastReceiver
	{
		public override void OnReceive (Context context, Intent intent)
		{
			(context.GetSystemService (Context.NotificationService) as NotificationManager).Cancel (intent.GetIntExtra ("NOTIF_ID", 0));
		}
	}
}

