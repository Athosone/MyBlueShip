using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MyBlueShip.Droid.Notifications
{
    public class ReceiveParkingResponseEvent : EventArgs
    {
        public Intent intent { get; set; }

		public ReceiveParkingResponseEvent()
		{
		}

        public ReceiveParkingResponseEvent(Intent i)
        {
            intent = i;
        }
    }
}