using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MyBlueShip.Droid.Notifications
{
	[BroadcastReceiver]
	public class ParkingResponseReceiver : NotificationReceiver
    {
        public ParkingResponseReceiver() { }
	
        public override void OnReceive(Context context, Intent i)
        {
			base.OnReceive (context, i);
			AppSingleton.Current.NewParkingResponse (i);

        }
    }
}