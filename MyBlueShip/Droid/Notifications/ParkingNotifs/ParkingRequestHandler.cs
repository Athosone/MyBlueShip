using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MyBlueShip.Droid.Notifications
{
    [BroadcastReceiver]
	[IntentFilter(new []  {"DENIED", "APPROVED"})]
	public class ParkingRequestHandler : NotificationReceiver
    {
		private Context context;
		private ulong toUserId;

		public override void OnReceive (Context context, Intent intent)
		{
			base.OnReceive (context, intent);
			this.context = context;
			string status = intent.GetStringExtra("REQUEST") ?? "Error";
			this.toUserId = Convert.ToUInt64(intent.GetFloatExtra("TOUSERID", 0.0f));
				
			if (this.toUserId == 0)
				return;
			switch (intent.Action)
			{
			case "APPROVED":
				ApproveParking();
				break;
			case "DENIED":
				DeniedParking();
				break;
			default:
				break;
			}
		}

        private async void DeniedParking()
        {
            Console.WriteLine("Parking Denied");
			Dictionary<string, string> dic = new Dictionary<string, string>(1);

			ulong uid = MMCHelpers.GetCurrentUserIdPrefs ();
			dic.Add("APP_Type", "1");
			dic.Add("action", "ParkingResponse");
			dic.Add("Response", "KO");
			dic.Add("FromIdUser", Convert.ToString(uid));
			List<ulong> ids = new List<ulong>(1);

			ids.Add(this.toUserId);
			await MBSApi.Instance.SendPush(ids, dic);
        }

        private async void ApproveParking()
        {
            Console.WriteLine("Parking Approved");
			Dictionary<string, string> dic = new Dictionary<string, string>(1);

			ulong uid = MMCHelpers.GetCurrentUserIdPrefs ();
			dic.Add("APP_Type", "1");
			dic.Add("action", "ParkingResponse");
			dic.Add("Response", "OK");
			dic.Add("FromIdUser", Convert.ToString(uid));
			List<ulong> ids = new List<ulong>(1);
			ids.Add(this.toUserId);
			await MBSApi.Instance.SendPush(ids, dic);
        }
    }
}