﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace MyBlueShip.Droid
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar", Label = "ActivityHorodateurOrEmplacement")]			
	public class ActivityHorodateurOrEmplacement : Activity
	{
		ImageView	imageViewMyHorodateur;
		ImageView	imageViewMyEmplacement;

		TextView	applicationTitle;
		TextView	myHorodateur;
		TextView	myEmplacement;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.ActivityHorodateurOrEmplacement);

			//set the view
			imageViewMyHorodateur = FindViewById<ImageView> (Resource.Id.myHorodateurImageView);
			imageViewMyEmplacement = FindViewById<ImageView> (Resource.Id.myEmplacementImageView);
			applicationTitle = FindViewById <TextView> (Resource.Id.applicationTitle);
			myHorodateur = FindViewById<TextView> (Resource.Id.myHorodateurString);
			myEmplacement = FindViewById<TextView> (Resource.Id.myEmplacementString);

			//set the constant
			applicationTitle.Text = Constants.StringApplication.APPLICATION_TITLE;
			myHorodateur.Text = Constants.StringApplication.MY_HORODATEUR;
			myEmplacement.Text = Constants.StringApplication.MY_EMPLACEMENT;

			//set the typeface
			Typeface tf = Typeface.CreateFromAsset (Application.Context.Assets, Constants.TypefaceInfos.MY_BLUE_SHIP_TYPEFACE);
			applicationTitle.Typeface = tf;
			myHorodateur.Typeface = tf;
			myEmplacement.Typeface = tf;

			//go to Activity ActivityMyHorodateur
			imageViewMyHorodateur.Click += (object sender, EventArgs e) => {
				StartActivity(typeof(ActivityHorodateur));
			};

			//go to Activity ActivityMyEmplacement
			imageViewMyEmplacement.Click += (object sender, EventArgs e) => {
				StartActivity(typeof(ActivityMyEmplacement));
			};

		}
	}
}

