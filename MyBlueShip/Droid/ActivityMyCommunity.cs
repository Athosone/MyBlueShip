﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Gms.Maps;
using Android.Locations;
using MyBlueShip.Library.Android.MAP;
using Android.Gms.Maps.Model;
using MyBlueShip.Common;
using MyBlueShip.API.DataModel;
using MyBlueShip.Droid.Notifications;
using MyBlueShip.Library.Android.Design;
//	[IntentFilter(new [] { Constants.PushNotfication.INTENT_FILTER_PARKING_RESPONSE })]
using Android.Content.PM;

namespace MyBlueShip.Droid
{
	[Activity (Theme = "@android:style/Theme.Holo.Light.NoActionBar", Label = "ActivityMyCommunity", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class ActivityMyCommunity : MapDataProvider, IOnMapReadyCallback
    {
		private List<KeyValuePair<string, MBSParking>> 	markerParking;
		private ParkingResponseReceiver 				receiver;
		private List<ulong> 							deniedUsersId;
		private MapFragment 							_mapFragment;
		private GoogleMap 								_map;
		private Location 								currentLoc;
        private string 									TAG = "ActivityMyCommunity ";
        private ulong 									acceptedUserId = 0;
		private int 									i = 0;

        protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.MapLayout);
            deniedUsersId = new List<ulong>();
			InitMapFragment();
            receiver = new ParkingResponseReceiver();
			AppSingleton.Current.parkingResponseEvent += new EventHandler<ReceiveParkingResponseEvent>(Receiver_parkingResponseEvent);
		}

        private void Receiver_parkingResponseEvent(object sender, ReceiveParkingResponseEvent e)
        {
            Intent intent = e.intent;
			ulong userid = (ulong)Convert.ToInt64(intent.GetFloatExtra("USER_ID", 0.0f));
            if (intent.Action == "PARKING_RESPONSE")
            {
                switch (intent.GetStringExtra("RESPONSE"))
                {
				case "OK":
					this.acceptedUserId = userid;
                        break;
				case "KO":
					this.deniedUsersId.Add (userid);
                        break;
                    default:
                        break;
                }
            }
        }


        public override void OnBackPressed(){
			StartActivity (typeof(ActivityMainPage));
            this.Finish();
		}

		protected override void OnResume()
		{
			base.OnResume();
            RegisterReceiver(receiver, new IntentFilter(Constants.PushNotfication.INTENT_FILTER_PARKING_RESPONSE));
        }

        private void InitMapFragment()
		{
			_mapFragment = FragmentManager.FindFragmentByTag("map") as MapFragment;
			if (_mapFragment == null)
			{
				GoogleMapOptions mapOptions = new GoogleMapOptions()
					.InvokeMapType(GoogleMap.MapTypeSatellite)
					.InvokeZoomControlsEnabled(false)
					.InvokeCompassEnabled(true);

				FragmentTransaction fragTx = FragmentManager.BeginTransaction();
				_mapFragment = MapFragment.NewInstance(mapOptions);
				fragTx.Add(Resource.Id.map, _mapFragment, "map");
				fragTx.Commit();
			}
            _mapFragment.GetMapAsync(this);
		}

        public void OnMapReady(GoogleMap googleMap)
        {
            this._map = googleMap;
            if (_map != null)
            {
                this._map.MapType = GoogleMap.MapTypeNormal;
                this._map.UiSettings.ZoomControlsEnabled = true;
                this._map.UiSettings.CompassEnabled = true;
                this._map.UiSettings.MyLocationButtonEnabled = true;
                _map.MyLocationEnabled = true;
                googleMap.MarkerClick += MapOnMarkerClick;
            }
        }

        protected override void DidUpdateNewLocation(Location location)
        {
            if (this._map != null && location != null)
            {
                this.currentLoc = location;
                CameraUpdate updatecam = this.MoveCameraToPosition(location);
                if (i < 2)
                {
                    this._map.MoveCamera(updatecam);
                    i += 1;
                }
                UpdateParkings();
            }
        }

        protected override void DidReceivedError(string error)
        {
            Console.WriteLine("ERROR MobMyCommunity: " + error);
        }

        private CameraUpdate MoveCameraToPosition(Location location)
        {
            CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
            builder.Target(new LatLng(location.Latitude, location.Longitude));
			builder.Zoom(Constants.LocationInfos.ZOOM_CAMERA);
            CameraPosition cameraPosition = builder.Build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
            return cameraUpdate;
        }

        public async void UpdateParkings()
        {
            try
            {
                List<MBSParking> parks = await MBSApi.Instance.GetCommnityUsers(this.lastLocation.Latitude, this.lastLocation.Longitude, 5000);
                _map.Clear();
                if (parks != null && parks.Count > 0)
                {
                   CreateMarkers(parks);
                }
            }
            catch (MBSExceptionApi e)
            {
                Console.WriteLine(TAG + "UpdateParking" + e.Message);
            }
        }

        public void CreateMarkers(List<MBSParking> parks)
        {
            if (this.markerParking == null)
                this.markerParking = new List<KeyValuePair<string, MBSParking>>();
            this.markerParking.Clear();
           
            foreach(MBSParking park in parks)
            {
                if (!this.deniedUsersId.Contains(park.id_user))
                {
					if (park.id_user == this.acceptedUserId) {
						Intent intent = new Intent(Intent.ActionView, 
							Android.Net.Uri.Parse("http://maps.google.com/maps?daddr=" + Convert.ToString(park.Latitude).Replace(",", ".") + "," + Convert.ToString(park.Longitude).Replace(",", ".")));
						this.acceptedUserId = 0;
						StartActivity(intent);
					}
                    MarkerOptions markerOpt = new MarkerOptions();
                    markerOpt.SetPosition(new LatLng(park.Latitude, park.Longitude));
                    //markerOpt.SetIcon(BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueGreen));
					markerOpt.SetIcon(BitmapDescriptorFactory.FromResource(MMCHelpers.GetResourceIdIconAvatar(park.id_icone ?? 0)));
                    markerOpt.Flat(true);
                    Marker mark = this._map.AddMarker(markerOpt);
                    this.markerParking.Add(new KeyValuePair<string, MBSParking>(mark.Id, park));
                }
            }
        }

        private void MapOnMarkerClick(object sender, GoogleMap.MarkerClickEventArgs markerClickEventArgs)
        {
            markerClickEventArgs.Handled = true;
            Marker marker = markerClickEventArgs.Marker;

            foreach (KeyValuePair<string, MBSParking> kvp in this.markerParking)
            {
                if (kvp.Key == marker.Id)
                {
                    sendNotifToParkedUser(kvp.Value);
                }
            }
        }

        private async void sendNotifToParkedUser(MBSParking park)
        {
            try
            {
                MBSTags tags = await MBSApi.Instance.GetTag(park.id_tag);
				Dictionary<string, string> dic = new Dictionary<string, string>(1);

				ulong uid = MMCHelpers.GetCurrentUserIdPrefs();
				dic.Add("APP_Type", "1");
				dic.Add("action", "ParkingRequest");
				dic.Add("FromIdUser", Convert.ToString(uid));
				List<ulong> ids = new List<ulong>(1);
				ids.Add(tags.id_user);
				

                await MBSApi.Instance.SendPush(ids, dic);
				MBSUser destUser = await MBSApi.Instance.GetUser(tags.id_user);
				ToastCustom.MakeText(this, "Votre demande est envoyée à: ", ToastLength.Short, destUser.Pseudo).Show();
            }
            catch (MBSExceptionApi e)
            {
                Console.WriteLine("Erreur lors de l'envoie de la notification: " + e.Message);
            }
         }
    }
}

